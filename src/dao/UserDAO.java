package dao;

import model.User;
import utils.DbManager;

import java.sql.*;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO {

    public Vector<User> getAllUsers() throws SQLException, ClassNotFoundException {

        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();
        int id;
        String email;
        String password;
        String firstName;
        String lastName;
        Date birthDate;
        Date createDate;
        String userType;

        Vector<User> userData = new Vector();

        String query = "select * from user";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                User tempUser = new User();
                id = (rs.getInt(1));
                email = (rs.getString(2));
                password = (rs.getString(3));
                firstName = (rs.getString(4));
                lastName = (rs.getString(5));
                birthDate = (rs.getDate(6));
                createDate = (rs.getDate(7));
                userType = (rs.getString(8));

                tempUser.setId(id);
                tempUser.setEmail(email);
                tempUser.setPassword(password);
                tempUser.setFirstName(firstName);
                tempUser.setLastName(lastName);
                tempUser.setBirthDate(birthDate);
                tempUser.setCreateDate(createDate);
                tempUser.setUserType(userType);
                userData.add(tempUser);
            }
        } catch (SQLException e) {
                e.printStackTrace();
        }

        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return userData;


    }

    // https://www.codejava.net/coding/jsp-servlet-jdbc-mysql-create-read-update-delete-crud-example
    public void addUser(User user) {
        try {

            // https://stackoverflow.com/questions/27177494/how-to-insert-date-into-database-using-jsp
            Date dob = user.getBirthDate();

            // https://stackoverflow.com/questions/13112811/how-to-insert-current-date-into-mysql-db-using-java
            java.sql.Timestamp createDate = new java.sql.Timestamp(new java.util.Date().getTime());

            DbManager dmbgr = new DbManager();
            Connection con = dmbgr.getConnection();
            PreparedStatement stmt =  con.prepareStatement("insert into fyp.user(Email,Password,FirstName,LastName,BirthDate,CreateDate,UserType) values (?, ?, ?, ?, ?, ?, ? )");
            // Parameters start with 1
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getFirstName());
            stmt.setString(4, user.getLastName());
            stmt.setDate(5, user.getBirthDate());
            stmt.setTimestamp(6, createDate);
            stmt.setString(7, user.getUserType());
            stmt.executeUpdate();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public User getUser(int tempId)
            throws SQLException, ClassNotFoundException {

        String sql = "select * from fyp.user where UserID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, tempId);
        ResultSet rs = stmt.executeQuery();
        User tempUser = new User();

        if (rs.next()) {
            int id = rs.getInt("UserID");
            String email = rs.getString("Email");
            String password = rs.getString("Password");
            String fName = rs.getString("FirstName");
            String lName = rs.getString("LastName");
            Date birthDate = rs.getDate("BirthDate");
            Date createDate = rs.getDate("CreateDate");
            String userType = rs.getString("UserType");

            tempUser.setId(id);
            tempUser.setEmail(email);
            tempUser.setPassword(password);
            tempUser.setFirstName(fName);
            tempUser.setLastName(lName);
            tempUser.setBirthDate(birthDate);
            tempUser.setCreateDate(createDate);
            tempUser.setUserType(userType);

        }
        con.close();
        return tempUser;
    }

    public void deleteUser(int id)
            throws SQLException, ClassNotFoundException {
        String sql = "delete from fyp.user where UserID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1,id);
        stmt.executeUpdate();
        con.close();

    }

    public int updateUser(User user)
            throws SQLException, ClassNotFoundException {

        String sql = "update fyp.user set FirstName=?, LastName=?, BirthDate=? where UserID=?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setString(1, user.getFirstName());
        stmt.setString(2, user.getLastName());
        stmt.setDate(3, user.getBirthDate());
        stmt.setInt(4, user.getId());

        // https://coderanch.com/t/304094/databases/return-type-executeUpdate
        int i = stmt.executeUpdate();
        con.close();

       return i;

    }

    /* http://www.ntu.edu.sg/home/ehchua/programming/java/DateTimeCalendar.html */

    public int[] getCreatedDays() throws SQLException, ClassNotFoundException {


        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();
        Calendar cal = Calendar.getInstance();
        int sun, mon, tues, wed, thurs, fri, sat;
        Date createDate;
        int[] dayCount = new int[6];
        System.out.println(dayCount);

        String query = "select * from user";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                createDate = (rs.getDate(7));
                java.sql.Date date = Date.valueOf(String.valueOf(createDate));
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_WEEK);



            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dayCount;

    }

}
