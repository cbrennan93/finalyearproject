package dao;

import model.User;
import utils.DbManager;

import java.sql.*;

/* https://krazytech.com/programs/session-role-based-java-login-example */

public class LoginDAO {

    public User authenticateUser(String email, String password) throws SQLException, ClassNotFoundException {

        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();

        int userId;
        String fName;
        String lName;
        Date birthDate;
        Date createDate;
        String userType;

        User tempUser = new User();

        String tempEmail;
        String tempPassword;

        String query = "select * from fyp.user";
        try
        {
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
                userId = rs.getInt("UserID");
                tempEmail = rs.getString("Email");
                tempPassword = rs.getString("Password");
                fName = rs.getString("FirstName");
                lName = rs.getString("LastName");
                birthDate = rs.getDate("BirthDate");
                createDate = rs.getDate("CreateDate");
                userType = rs.getString("UserType");

                tempUser.setId(userId);
                tempUser.setEmail(email);
                tempUser.setPassword(password);
                tempUser.setFirstName(fName);
                tempUser.setLastName(lName);
                tempUser.setBirthDate(birthDate);
                tempUser.setCreateDate(createDate);
                tempUser.setUserType(userType);

                if(email.equals(tempEmail) && password.equals(tempPassword) && userType.equals("Admin"))
                    return tempUser;
                else if(email.equals(tempEmail) && password.equals(tempPassword) && userType.equals("Editor"))
                    return tempUser;
                else if(email.equals(tempEmail) && password.equals(tempPassword) && userType.equals("RegUser"))
                    return tempUser;
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

        con.close();
        return null;

    }
}
