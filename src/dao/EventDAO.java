package dao;

import model.Event;
import model.Stage;
import utils.DbManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EventDAO {

    public List<Event> getEvents(int businessId) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.event " +
                "WHERE business_BusinessID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, businessId);

        ResultSet rs = stmt.executeQuery();
        List<Event> events = new ArrayList<>();

        while (rs.next()) {
            int id = rs.getInt("EventID");
            String name = rs.getString("EventName");
            String desc = rs.getString("EventDescription");
            Date date = rs.getDate("EventDate");
            int part = rs.getInt("Participants");

            Event e = new Event();
            e.setId(id);
            e.setName(name);
            e.setDesc(desc);
            e.setDate(date);
            e.setParticipants(part);
            e.setBusinessId(businessId);
            events.add(e);

        }
        /*
        https://stackoverflow.com/questions/5927109/sort-objects-in-arraylist-by-date
        IntelliJ auto-corrected to this method from the one at the link above
         */
        events.sort(Comparator.comparing(Event::getDate));
        // https://stackoverflow.com/questions/22604952/error-data-source-rejected-establishment-of-connection-message-from-server-t

        con.close();

        return events;

    }

    public void addEvent(Event event) {

        try {
            Date date = event.getDate();
            Timestamp ts = new Timestamp(date.getTime());

            String sql = "insert into fyp.event(EventName, EventDescription, EventDate, Participants, business_BusinessID) values (?, ?, ?, ?, ?)";

            DbManager dmbgr = new DbManager();
            Connection con = dmbgr.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            // Parameters start with 1
            stmt.setString(1, event.getName());
            stmt.setString(2, event.getDesc());
            stmt.setTimestamp(3, ts);
            stmt.setInt(4, event.getParticipants());
            stmt.setInt(5, event.getBusinessId());
            stmt.executeUpdate();

            int eventId = 0;
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                eventId = rs.getInt(1);
            }
            stmt.close();

            createFirstStage(eventId, event.getBusinessId());

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Event getEventById(int eventId) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.event " +
                "WHERE EventID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, eventId);

        ResultSet rs = stmt.executeQuery();
        Event event = new Event();

        while (rs.next()) {
            String name = rs.getString("EventName");
            String desc = rs.getString("EventDescription");
            Date date = rs.getDate("EventDate");
            int part = rs.getInt("Participants");
            int businessId = rs.getInt("business_BusinessID");

            event.setId(eventId);
            event.setName(name);
            event.setDesc(desc);
            event.setDate(date);
            event.setParticipants(part);
            event.setBusinessId(businessId);

        }
        con.close();
        return event;

    }

    private void createFirstStage(int eventId, int businessId) throws SQLException, ClassNotFoundException {

        String sql = "insert into fyp.stage(StageNumber,event_eventID,business_businessID) values (?, ?, ?)";
        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, 1);
        stmt.setInt(2, eventId);
        stmt.setInt(3, businessId);
        stmt.executeUpdate();
        stmt.close();

    }

    public List<Stage> getStages(int eventId) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.stage " +
                "WHERE event_eventID = ?";
        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, eventId);

        ResultSet rs = stmt.executeQuery();
        List<Stage> stages = new ArrayList<>();

        while (rs.next()) {
            int stageId = rs.getInt("StageID");
            int stageNumber = rs.getInt("StageNumber");
            int businessId = rs.getInt("business_BusinessID");

            Stage s = new Stage();
            s.setId(stageId);
            s.setStageNumber(stageNumber);
            s.setEventId(eventId);
            s.setBusinessId(businessId);
            stages.add(s);

        }
        con.close();
        return stages;
    }

    public int createOtherStage(int stageNumber, int eventId, int businessId) throws SQLException, ClassNotFoundException {

        String sql = "insert into fyp.stage(StageNumber,event_eventID,business_businessID) values (?, ?, ?)";
        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, stageNumber);
        stmt.setInt(2, eventId);
        stmt.setInt(3, businessId);
        int i = stmt.executeUpdate();
        stmt.close();
        return i;
    }

    public int updateParticipants(int part, int eventId) throws SQLException, ClassNotFoundException {

        String sql = "update fyp.event set Participants=? where EventID = ?";
        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, part);
        stmt.setInt(2, eventId);

        // https://coderanch.com/t/304094/databases/return-type-executeUpdate
        int i = stmt.executeUpdate();
        con.close();

        return i;

    }
}
