package dao;

import model.Business;
import utils.DbManager;

import java.math.BigDecimal;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class BusinessDAO {

    public Vector<Business> getAllBusiness(int tempId) throws SQLException, ClassNotFoundException {
        DbManager dmbgr = new DbManager();
        Connection con = dmbgr.getConnection();
        int id;
        String name;
        String address;
        String placeId;
        BigDecimal lat;
        BigDecimal lng;
        Date createDate;
        int userId;
        String query;

        Vector<Business> businessData = new Vector();

        if (tempId == 0) {
            query = "select * from business";
        } else {
            query = "select * from business where user_UserID=" + "'" + tempId + "'";
        }
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Business b = new Business();
                id = rs.getInt("BusinessID");
                name = rs.getString("BusinessName");
                address = rs.getString("BusinessAddress");
                lat = rs.getBigDecimal("Latitude");
                lng = rs.getBigDecimal("Longitude");
                placeId = rs.getString("PlaceIDMaps");
                createDate = rs.getDate("CreateDate");
                userId = rs.getInt("user_UserID");

                b.setId(id);
                b.setName(name);
                b.setAddress(address);
                b.setLng(lng);
                b.setLat(lat);
                b.setPlaceIdMaps(placeId);
                b.setCreateDate(createDate);
                b.setUserId(userId);
                businessData.add(b);

            }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        // https://stackoverflow.com/questions/22604952/error-data-source-rejected-establishment-of-connection-message-from-server-t
        finally {
            if (con!= null) {
                con.close();
            }
        }
        return businessData;
    }

    public void addBusiness(Business business) {
        try {

            // https://stackoverflow.com/questions/13112811/how-to-insert-current-date-into-mysql-db-using-java
            java.sql.Timestamp createDate = new java.sql.Timestamp(new java.util.Date().getTime());

            DbManager dmbgr = new DbManager();
            Connection con = dmbgr.getConnection();
            PreparedStatement stmt =  con.prepareStatement("insert into fyp.business(BusinessName,BusinessAddress,Latitude,Longitude,PlaceIDMaps,CreateDate,user_UserID) values (?, ?, ?, ?, ?, ?, ?)");
            // Parameters start with 1
            stmt.setString(1, business.getName());
            stmt.setString(2, business.getAddress());
            stmt.setBigDecimal(3, business.getLat());
            stmt.setBigDecimal(4, business.getLng());
            stmt.setString(5, business.getPlaceIdMaps());
            stmt.setTimestamp(6, createDate);
            stmt.setInt(7, business.getUserId());
            stmt.executeUpdate();
            con.close();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Business getBusiness(int id) throws SQLException, ClassNotFoundException {

        Business business = null;
        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        String sql = "SELECT * " +
                "FROM fyp.business " +
                "WHERE BusinessID = ?";
        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {

            String name = rs.getString("BusinessName");
            String address = rs.getString("BusinessAddress");
            BigDecimal lat = rs.getBigDecimal("Latitude");
            BigDecimal lng = rs.getBigDecimal("Longitude");
            String placeIdMaps = rs.getString("PlaceIDMaps");
            Date createDate = rs.getDate("CreateDate");
            int userId = rs.getInt("user_UserID");
            business = new Business(id, name, address, lat, lng, placeIdMaps, createDate, userId);
            con.close();

        }

        return business;
    }

    public int updateBusiness(Business b)
            throws SQLException, ClassNotFoundException {

        String sql = "update fyp.business set BusinessName=?, BusinessAddress=?, Latitude=?, Longitude=?, PlaceIDMaps=? where BusinessID=?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setString(1, b.getName());
        stmt.setString(2, b.getAddress());
        stmt.setBigDecimal(3,b.getLat());
        stmt.setBigDecimal(4, b.getLng());
        stmt.setString(5, b.getPlaceIdMaps());
        stmt.setInt(6, b.getId());

        // https://coderanch.com/t/304094/databases/return-type-executeUpdate
        int i = stmt.executeUpdate();
        con.close();

        return i;

    }

    public void deleteBusiness(int id)
            throws SQLException, ClassNotFoundException {
        String sql = "delete from fyp.business where BusinessID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1,id);
        stmt.executeUpdate();
        con.close();

    }

}

