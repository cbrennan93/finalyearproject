package dao;

import model.Menu;
import utils.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MenuDAO {

    public List<Menu> getMenu(int id) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.menu " +
                "WHERE business_BusinessID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();
        List<Menu> menus = new ArrayList<>();

        while (rs.next()) {

            int menuId = rs.getInt("MenuID");
            String name = rs.getString("MenuName");

            Menu m = new Menu(menuId, name, id);
            menus.add(m);

        }
        con.close();
        return menus;

    }

    public Menu getMenuById(int id) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.menu " +
                "WHERE MenuID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();

        Menu menu = new Menu();
        while (rs.next()) {

            int businessId = rs.getInt("business_BusinessID");
            String name = rs.getString("MenuName");

            menu.setId(id);
            menu.setMenuName(name);
            menu.setBusinessId(businessId);

        }
        con.close();
        return menu;

    }

    public void addMenu(Menu m) {

        try {

            DbManager dmbgr = new DbManager();
            Connection con = dmbgr.getConnection();
            PreparedStatement stmt =  con.prepareStatement("insert into fyp.menu(MenuName,business_BusinessID) values (?, ?)");
            // Parameters start with 1
            stmt.setString(1, m.getMenuName());
            stmt.setInt(2, m.getBusinessId());
            stmt.executeUpdate();
            con.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public int updateMenu(Menu m)
            throws SQLException, ClassNotFoundException {

        String sql = "update fyp.menu set MenuName=?, business_BusinessID=? where MenuID=?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setString(1, m.getMenuName());
        stmt.setInt(2, m.getBusinessId());
        stmt.setInt(3, m.getId());

        // https://coderanch.com/t/304094/databases/return-type-executeUpdate
        int i = stmt.executeUpdate();
        con.close();

        return i;

    }

    public void deleteMenu(int id)
            throws SQLException, ClassNotFoundException {
        String sql = "delete from fyp.menu where MenuID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1,id);
        stmt.executeUpdate();
        con.close();
    }
}
