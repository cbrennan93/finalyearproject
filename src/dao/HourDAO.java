package dao;

import model.Hour;
import model.Menu;
import utils.DbManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HourDAO {

    public int addHours(List<Hour> hourList, int businessId) throws SQLException, ClassNotFoundException {

        int check = checkHours(businessId);
        int i = 0;

        if (check < 1 ) {

            try {

                String sql = "insert into fyp.hours(Day, OpenTime, CloseTime, business_BusinessID) values (?, ?, ?, ?)";

                DbManager dmbgr = new DbManager();
                Connection con = dmbgr.getConnection();
                PreparedStatement stmt = con.prepareStatement(sql);

                for (Hour hour : hourList) {
                    // Parameters start with 1
                    stmt.setString(1, hour.getDay());
                    stmt.setString(2, hour.getOpen());
                    stmt.setString(3, hour.getClose());
                    stmt.setInt(4, hour.getBusinessId());
                    i = stmt.executeUpdate();
                }

                stmt.close();

            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return i;

    }

    private int checkHours(int businessId) throws SQLException, ClassNotFoundException {

        String sqlCheck = "SELECT COUNT(business_BusinessID) AS number FROM fyp.hours WHERE business_BusinessID = ?";
        int i = 0;

        DbManager dmbgrCheck = new DbManager();
        Connection conCheck = dmbgrCheck.getConnection();
        PreparedStatement stmtCheck = conCheck.prepareStatement(sqlCheck);
        stmtCheck.setInt(1, businessId);
        ResultSet rs = stmtCheck.executeQuery();
        if(rs.next()){
            System.out.println(rs.getInt(1));
            i = rs.getInt(1);
        }
        conCheck.close();
        return i;
    }

    public List<Hour> getHours(int id) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.hours " +
                "WHERE business_BusinessID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();
        List<Hour> hours = new ArrayList<>();

        while (rs.next()) {

            int hourId = rs.getInt("BusinessHoursID");
            String day = rs.getString("Day");
            String open = rs.getString("OpenTime");
            String close = rs.getString("CloseTime");

            Hour h = new Hour(hourId, day, open, close, id);
            hours.add(h);

        }
        con.close();
        return hours;

    }
}
