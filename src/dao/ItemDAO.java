package dao;

import model.Item;
import utils.DbManager;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemDAO {

    public List<Item> getItem(int id) throws SQLException, ClassNotFoundException {

        String sql = "SELECT *" +
                "FROM fyp.item " +
                "WHERE menu_MenuID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();

        PreparedStatement stmt = con.prepareStatement(sql);

        stmt.setInt(1, id);

        ResultSet rs = stmt.executeQuery();
        List<Item> items = new ArrayList<>();

        while (rs.next()) {

            int itemId = rs.getInt("ItemID");
            String name = rs.getString("ItemName");
            String desc = rs.getString("ItemDescription");
            String cat = rs.getString("ItemCategory");
            BigDecimal price = rs.getBigDecimal("ItemPrice");
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);

            Item i = new Item(itemId, name, desc, cat, price, id);
            items.add(i);

        }
        con.close();
        return items;

    }

    public void addItem(Item i) {

        try {

            DbManager dmbgr = new DbManager();
            Connection con = dmbgr.getConnection();
            String sql = "insert into fyp.item(ItemName, ItemDescription, ItemCategory, ItemPrice, menu_MenuID) values (?, ?, ?, ?, ?)";
            PreparedStatement stmt =  con.prepareStatement(sql);

            stmt.setString(1, i.getItemName());
            stmt.setString(2, i.getItemDesc());
            stmt.setString(3, i.getItemCat());
            BigDecimal price = i.getItemPrice();
            price = price.setScale(4, BigDecimal.ROUND_HALF_UP);
            stmt.setBigDecimal(4, price);
            stmt.setInt(5, i.getMenuId());
            stmt.executeUpdate();
            con.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public int updateItem(Item item)
            throws SQLException, ClassNotFoundException {

            String sql = "update fyp.item set ItemName=?, ItemDescription=?, ItemCategory=?, ItemPrice=? where ItemID=?";

            DbManager dbmgr = new DbManager();
            Connection con = dbmgr.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);

            stmt.setString(1, item.getItemName());
            stmt.setString(2, item.getItemDesc());
            stmt.setString(3, item.getItemCat());
            BigDecimal price = item.getItemPrice();
            price = price.setScale(4, BigDecimal.ROUND_HALF_UP);
            stmt.setBigDecimal(4, price);
            stmt.setInt(5, item.getId());

            // https://coderanch.com/t/304094/databases/return-type-executeUpdate
            int i = stmt.executeUpdate();
            con.close();

            return i;

    }

    public void deleteItem(int id)
            throws SQLException, ClassNotFoundException {
        String sql = "delete from fyp.item where ItemID = ?";

        DbManager dbmgr = new DbManager();
        Connection con = dbmgr.getConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1,id);
        stmt.executeUpdate();
        con.close();

    }
}
