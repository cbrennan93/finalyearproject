package manager;

import dao.BusinessDAO;
import dao.HourDAO;
import dao.ItemDAO;
import dao.MenuDAO;
import model.Business;
import model.Hour;
import model.Item;
import model.Menu;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class BusinessManager {

    public Business getBusiness(int id) throws SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        MenuDAO mDAO = new MenuDAO();
        HourDAO hDAO = new HourDAO();
        Business b = bDAO.getBusiness(id);
        List<Menu> m = mDAO.getMenu(id);
        List<Menu> menus = new ArrayList<>();
        List<Hour> hours = hDAO.getHours(id);

        // https://stackoverflow.com/questions/11177348/how-to-add-element-in-list-while-iterating-in-java
        for(int i = 0; i < m.size(); i++) {
            Menu menu =  m.get(i);
            int menuId = menu.getId();
            ItemDAO iDAO = new ItemDAO();
            List<Item> items = iDAO.getItem(menuId);
            menu = new Menu(menu.getId(), menu.getMenuName(), menu.getBusinessId(), items);
            menus.add(menu);
        }

        b = new Business(b.getId(), b.getName(), b.getAddress(), b.getLat(), b.getLng(), b.getPlaceIdMaps(), b.getCreateDate(), b.getUserId(), menus, hours);

        return b;
    }

    public List<Business> getBusinessMenu(int userId) throws SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        Vector<Business> businessVector = bDAO.getAllBusiness(userId);
        // https://www.geeksforgeeks.org/vector-vs-arraylist-java/
        List<Business> b = Collections.list(businessVector.elements());
        List<Business> businesses = new ArrayList<>();

        for(int i = 0; i < b.size(); i++) {
            Business business =  b.get(i);
            int businessId = business.getId();
            MenuDAO mDAO = new MenuDAO();
            List<Menu> menus = mDAO.getMenu(businessId);
            if (menus.size() > 0) {
                business = new Business(business.getId(), business.getName(), business.getAddress(), business.getLat(), business.getLng(),
                        business.getPlaceIdMaps(), business.getCreateDate(), business.getUserId(), menus);
                businesses.add(business);
            }
        }

        return businesses;

    }

    public List<Business> getBusinessHour(int userId) throws SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        Vector<Business> businessVector = bDAO.getAllBusiness(userId);
        // https://www.geeksforgeeks.org/vector-vs-arraylist-java/
        List<Business> b = Collections.list(businessVector.elements());
        List<Business> businesses = new ArrayList<>();

        for(int i = 0; i < b.size(); i++) {
            Business business =  b.get(i);
            int businessId = business.getId();
            HourDAO hDAO = new HourDAO();
            List<Hour> hours = hDAO.getHours(businessId);
            if (hours.size() > 0) {
                business = new Business(business.getId(), business.getName(), business.getUserId(), hours);
                businesses.add(business);
            }
        }

        return businesses;

    }

}
