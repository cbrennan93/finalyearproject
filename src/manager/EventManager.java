package manager;

import dao.BusinessDAO;
import dao.EventDAO;
import model.Business;
import model.Event;
import model.Stage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class EventManager {

    public List<Business> getBusinessEvent() throws SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        Vector<Business> businessVector = bDAO.getAllBusiness(0);
        // https://www.geeksforgeeks.org/vector-vs-arraylist-java/
        List<Business> b = Collections.list(businessVector.elements());
        List<Business> businesses = new ArrayList<>();

        //IntelliJ correction to foreach from for
        for (Business business : b) {
            int businessId = business.getId();
            EventDAO eDAO = new EventDAO();
            List<Event> events = eDAO.getEvents(businessId);
            if (events.size() > 0) {
                business = new Business(business.getId(), business.getName(), business.getAddress(), business.getLat(), business.getLng(),
                        business.getPlaceIdMaps(), business.getCreateDate(), events);
                businesses.add(business);
            }
        }

        return businesses;

    }

    public int manageStage(int eventId, int part, int businessId) throws SQLException, ClassNotFoundException {

        EventDAO eDAO = new EventDAO();
        List<Stage> s = eDAO.getStages(eventId);
        int iOne;
        int iTwo = 0;

        Stage lastStage = s.get(s.size() - 1);
        int lastStageNumber = lastStage.getStageNumber();

        int newStageNumber = lastStageNumber + 1;
        if (part > 1) {

            iOne = eDAO.createOtherStage(newStageNumber, eventId, businessId);
            if (iOne > 0) {
                int newPart = part - 1;
                iTwo = eDAO.updateParticipants(newPart, eventId);
            } else {
                iTwo = iOne;
            }

        }

        return iTwo;

    }

    public Event getEventById(int eventId) throws SQLException, ClassNotFoundException {

        EventDAO eDAO = new EventDAO();
        Event e = eDAO.getEventById(eventId);

        List<Stage> s = eDAO.getStages(eventId);
        List<Stage> stages = new ArrayList<>();
        Event event;

        //IntelliJ correction to foreach from for
        for (Stage stage : s) {
            int businessId = stage.getBusinessId();
            BusinessDAO bDAO = new BusinessDAO();
            Business business = bDAO.getBusiness(businessId);
            stage = new Stage(stage.getId(), stage.getStageNumber(), stage.getEventId(), stage.getBusinessId(), business);
            stages.add(stage);
        }

        event = new Event(e.getId(), e.getName(), e.getDesc(), e.getDate(), e.getParticipants(), e.getBusinessId(), stages);

        return event;

    }

}


