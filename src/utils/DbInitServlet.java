package utils;
import javax.servlet.http.HttpServlet;
import java.sql.SQLException;

public class DbInitServlet extends HttpServlet {

    //Set up and initialize the database, this is called on load in the web.xml file
    //3rd year java project
    public void init() {
        DbManager mngr = new DbManager();
        try {
            mngr.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

}
