package utils;

public interface IConstants {

    //3rd year java project
    //User Types
    String USER_TYPE_ADMIN = "Admin";
    String USER_TYPE_GENERAL_USER = "Editor";
    String USER_TYPE_REGISTERED_USER = "RegUser";

    //Session Keys
    String SESSION_KEY_USER = "SKUSER";
    String SESSION_KEY_ALL_USERS = "SKALLUSERS";
    String SESSION_KEY_BUSINESS = "SKBUSINESS";
    String SESSION_KEY_ALL_BUSINESS = "SKALLBUSINESS";
    String SESSION_KEY_ALL_BUSINESS_INDEX = "SKALLBUSINESSINDEX";

}
