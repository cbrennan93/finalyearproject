package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbManager {

    //https://stackoverflow.com/questions/34189756/warning-about-ssl-connection-when-connecting-to-mysql-database
    private String url = "jdbc:mysql://localhost:3306/fyp?autoReconnect=true&useSSL=false";
    private String user = "root";
    private String password = "myn@me!s8";

    // 3rd year java project
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection(url, user, password);
        return conn;
    }
 }

