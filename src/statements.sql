/* This is a dummy sql file for keeping track of my testing */

/* DO NOT RUN */

/* dummy create statement */
CREATE TABLE `fyp`.`user` (
  `UserID` INT NOT NULL AUTO_INCREMENT,
  `Email` VARCHAR(255) NOT NULL,
  `Password` VARCHAR (255) NOT NULL,
  `FirstName` VARCHAR(255) NULL,
  `LastName` VARCHAR(255) NULL,
  `BirthDate` DATETIME NOT NULL,
  `CreateDate` DATETIME NOT NULL,
  `UserType` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`UserID`));

/* dummy insert statements */
insert into fyp.user (Email, Password, FirstName, LastName, BirthDate, CreateDate, UserType)
values ('user1@gmail.com', 'mypassword', 'John', 'Doe', '1990-10-28', Now(), 'RegUser');

insert into fyp.user (Email, Password, FirstName, LastName, BirthDate, CreateDate, UserType)
values ('admin1@gmail.com', 'mypassword', 'Jane', 'Doe', '1960-01-13', Now(), 'Admin');

insert into fyp.item(ItemName, ItemDescription, ItemCategory, ItemPrice, menu_MenuID)
values('Mighty B.L.T', 'toasted bap with bacon, lettuce and tomato', 'Sandwich', 5.95, 1);

insert into fyp.item(ItemName, ItemDescription, ItemCategory, ItemPrice, menu_MenuID)
values('Toasted Special Sandwich', 'with salad and chips', 'Sandwich', 9.50, 1);

insert into fyp.item(ItemName, ItemDescription, ItemCategory, ItemPrice, menu_MenuID)
values('Deep Fried Scampi', 'with tartar sauce, salad and chips', 'Fish', 16.75, 2);

Select * from fyp.user;

SELECT * FROM fyp.business;

SELECT * FROM fyp.menu;

SELECT * FROM fyp.item;

SELECT * FROM fyp.event;

SELECT * FROM fyp.stage;

SELECT * FROM fyp.hours;

SELECT COUNT(business_BusinessID) AS number FROM fyp.hours
WHERE business_BusinessID = 1;

SELECT user.FirstName, user.LastName, business.BusinessName
FROM fyp.user
       RIGHT JOIN business ON user.UserID = business.user_UserID
ORDER BY FirstName;

SELECT *
FROM fyp.business b
       RIGHT JOIN event e on b.BusinessID = e.business_BusinessID;

SELECT *
FROM fyp.menu m
       RIGHT JOIN item i on m.MenuID = i.menu_MenuID;

ALTER TABLE fyp.item MODIFY COLUMN ItemID INT NOT NULL AUTO_INCREMENT;

ALTER TABLE fyp.item MODIFY COLUMN ItemPrice DECIMAL(13, 4);

ALTER TABLE fyp.hours MODIFY COLUMN BusinessHoursID INT NOT NULL AUTO_INCREMENT;

ALTER TABLE hours MODIFY OpenTime varchar(45);

ALTER TABLE hours MODIFY CloseTime varchar(45);

ALTER TABLE fyp.business
  ADD FOREIGN KEY (user_UserID)
REFERENCES fyp.user(UserID)
  ON DELETE CASCADE;

ALTER TABLE fyp.menu
  ADD FOREIGN KEY (business_BusinessID)
REFERENCES fyp.business(BusinessID)
  ON DELETE CASCADE;

ALTER TABLE fyp.item
  ADD FOREIGN KEY (menu_MenuID)
REFERENCES fyp.menu(MenuID)
  ON DELETE CASCADE;

ALTER TABLE fyp.stage
  ADD FOREIGN KEY (business_BusinessID)
REFERENCES fyp.business(BusinessID)
  ON DELETE CASCADE;

ALTER TABLE fyp.stage
  ADD FOREIGN KEY (event_eventID)
REFERENCES fyp.event(EventID)
  ON DELETE CASCADE;

ALTER TABLE fyp.hours
  ADD FOREIGN KEY (business_BusinessID)
REFERENCES fyp.business(BusinessID)
  ON DELETE CASCADE;

SELECT * FROM fyp.menu;

UPDATE fyp.menu
SET MenuName = 'Lunch'
WHERE MenuID = 2;

/* https://stackoverflow.com/questions/7363930/how-to-select-a-single-record-in-a-left-join */
SELECT b.BusinessName, b.BusinessAddress, b.CreateDate, b.user_UserID, m.MenuID, m.MenuName
FROM business b
       LEFT JOIN menu m ON m.business_BusinessID =
                           (SELECT MIN(business_BusinessID)
                            FROM menu
                            WHERE business_BusinessID = b.BusinessID)
WHERE b.BusinessID = 1;

SELECT b.BusinessID, b.BusinessName, b.BusinessAddress, m.MenuID, m.MenuName, i.ItemName, i.ItemDescription, ItemCategory, ItemPrice
FROM fyp.business b, fyp.menu m, fyp.item i
WHERE b.BusinessID = m.business_BusinessID
  AND m.MenuID = i.menu_MenuID
  AND b.BusinessID = 1;

/* https://stackoverflow.com/questions/13134913/figure-out-if-a-table-has-a-delete-on-cascade */
SELECT * FROM information_schema.REFERENTIAL_CONSTRAINTS;

/* http://www.mysqltutorial.org/mysql-add-column/ */
ALTER TABLE fyp.business
  ADD COLUMN PlaceIDMaps VARCHAR(200) AFTER BusinessAddress;

/* https://stackoverflow.com/questions/12504208/what-mysql-data-type-should-be-used-for-latitude-longitude-with-8-decimal-places */
ALTER TABLE fyp.business
  ADD COLUMN Latitude DECIMAL(10,8) AFTER BusinessAddress;

ALTER TABLE fyp.business
  ADD COLUMN Longitude DECIMAL(11,8) AFTER Latitude;

ALTER TABLE fyp.event
  ADD COLUMN EventDate DATETIME AFTER EventDescription;

ALTER TABLE fyp.event
  MODIFY EventID int(11) NOT NULL auto_increment;

ALTER TABLE fyp.event
    ADD COLUMN Participants int(11) NOT NULL AFTER EventDate;


UPDATE fyp.event
    SET Participants = 1
WHERE business_BusinessID = 1;

DELETE FROM fyp.event
WHERE EventID =   5;

/* Create statement - lost database on 26/02/2019 at 12:30pm  */
create table fyp.user
(
  UserID     int auto_increment
    primary key,
  Email      varchar(255) not null,
  Password   varchar(255) not null,
  FirstName  varchar(255) null,
  LastName   varchar(255) null,
  BirthDate  date         not null,
  CreateDate datetime     not null,
  UserType   varchar(45)  not null
);

create table fyp.business
(
  BusinessID      int auto_increment
    primary key,
  BusinessName    varchar(225)   not null,
  BusinessAddress varchar(225)   not null,
  Latitude        decimal(10, 8) null,
  Longitude       decimal(11, 8) null,
  PlaceIDMaps     varchar(200)   null,
  CreateDate      datetime       not null,
  user_UserID     int            not null,
  constraint fk_business_user1
  foreign key (user_UserID) references user (userid)
    on delete cascade
);

create index business_user1_idx
  on business (user_UserID);

create table fyp.event
(
  EventID             int auto_increment
    primary key,
  EventName           varchar(225) not null,
  EventDescription    text         null,
  EventDate           datetime     null,
  Participants        int          not null,
  business_BusinessID int          not null,
  constraint fk_event_business1
  foreign key (business_BusinessID) references business (businessid)
    on delete cascade
);

create index fk_event_business1_idx
  on event (business_BusinessID);

create table fyp.hours
(
  BusinessHoursID     int         not null
    primary key,
  Day                 varchar(45) not null,
  OpenTime            time        not null,
  CloseTime           time        not null,
  business_BusinessID int         not null,
  constraint fk_businesshours_business1
  foreign key (business_BusinessID) references business (businessid)
);

create index fk_businesshours_business1_idx
  on hours (business_BusinessID);

create table fyp.menu
(
  MenuID              int auto_increment
    primary key,
  MenuName            varchar(45) not null,
  business_BusinessID int         not null,
  constraint fk_menu_businness1
  foreign key (business_BusinessID) references business (businessid)
    on delete cascade
);

create table fyp.item
(
  ItemID          int auto_increment
    primary key,
  ItemName        varchar(225)   null,
  ItemDescription text           null,
  ItemCategory    varchar(225)   null,
  ItemPrice       decimal(13, 4) null,
  menu_MenuID     int            not null,
  constraint fk_item_menu1
  foreign key (menu_MenuID) references menu (menuid)
    on delete cascade
);

create index fk_item_menu1_idx
  on item (menu_MenuID);

create index fk_menu_business1_idx
  on menu (business_BusinessID);

create table fyp.stage
(
  StageID             int auto_increment
    primary key,
  StageNumber         int not null,
  event_eventID       int not null,
  business_businessID int not null,
  constraint fk_stage_business1
  foreign key (business_businessID) references business (businessid),
  constraint fk_stage_event1
  foreign key (event_eventID) references event (eventid)
    on delete cascade
);

create index fk_stage_business1_idx
  on stage (business_businessID);

create index fk_stage_event1_idx
  on stage (event_eventID);

create table fyp.usertest
(
  UserID       int auto_increment
    primary key,
  Email        varchar(255) not null,
  PasswordHash binary(64)   not null,
  Salt         char(38)     not null,
  FirstName    varchar(255) null,
  LastName     varchar(255) null,
  BirthDate    datetime     not null,
  CreateDate   datetime     not null,
  UserType     varchar(45)  not null
);


