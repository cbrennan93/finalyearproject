package model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public class Business {

    private int id;
    private String name;
    private String address;
    private BigDecimal lat;
    private BigDecimal lng;
    private String placeIdMaps;
    private Date createDate;
    private int userId;
    private List<Menu> menu;
    private List<Event> event;
    private List<Hour> hours;

    public Business() {
        this.setId(0);
        this.setName("Empty");
        this.setAddress("Empty");
        this.setCreateDate(null);
        this.setUserId(0);
    }

    public Business(int id, String name, String address, BigDecimal lat, BigDecimal lng, String placeIdMaps, Date createDate, int userId) {
        this.setId(id);
        this.setName(name);
        this.setAddress(address);
        this.setLat(lat);
        this.setLng(lng);
        this.setPlaceIdMaps(placeIdMaps);
        this.setCreateDate(createDate);
        this.setUserId(userId);
    }

    public Business(int id, String name, String address, BigDecimal lat, BigDecimal lng, String placeIdMaps, Date createDate, int userId, List<Menu> menu) {
        this.setId(id);
        this.setName(name);
        this.setAddress(address);
        this.setLat(lat);
        this.setLng(lng);
        this.setPlaceIdMaps(placeIdMaps);
        this.setCreateDate(createDate);
        this.setUserId(userId);
        this.setMenu(menu);
    }

    public Business(int id, String name, String address, BigDecimal lat, BigDecimal lng, String placeIdMaps, Date createDate, int userId, List<Menu> menu,  List<Hour> hours) {
        this.setId(id);
        this.setName(name);
        this.setAddress(address);
        this.setLat(lat);
        this.setLng(lng);
        this.setPlaceIdMaps(placeIdMaps);
        this.setCreateDate(createDate);
        this.setUserId(userId);
        this.setMenu(menu);
        this.setHours(hours);
    }

    public Business(int id, String name, String address, BigDecimal lat, BigDecimal lng, String placeIdMaps, Date createDate, List<Event> event) {
        this.setId(id);
        this.setName(name);
        this.setAddress(address);
        this.setLat(lat);
        this.setLng(lng);
        this.setPlaceIdMaps(placeIdMaps);
        this.setCreateDate(createDate);
        this.setEvent(event);
    }

    public Business(int id, String name, int userId, List<Hour> hours) {
        this.setId(id);
        this.setName(name);
        this.setUserId(userId);
        this.setHours(hours);
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public BigDecimal getLat() { return lat; }

    public void setLat(BigDecimal lat) { this.lat = lat; }

    public BigDecimal getLng() { return lng; }

    public void setLng(BigDecimal lng) { this.lng = lng; }

    public String getPlaceIdMaps() { return placeIdMaps; }

    public void setPlaceIdMaps(String placeIdMaps) { this.placeIdMaps = placeIdMaps; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) { this.createDate = createDate; }

    public int getUserId() { return userId; }

    public void setUserId(int userId) { this.userId = userId; }

    public List<Menu> getMenu() { return menu; }

    public void setMenu(List<Menu> menu) { this.menu = menu; }

    public List<Event> getEvent() { return event; }

    public void setEvent(List<Event> event) { this.event = event; }

    public List<Hour> getHours() { return hours; }

    public void setHours(List<Hour> hours) { this.hours = hours; }
}
