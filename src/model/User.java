package model;

import java.sql.Date;

public class User {

    private int id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Date createDate;
    private String userType;

    public User() {
        this.setId(0);
        this.setEmail("Empty");
        this.setPassword("Empty");
        this.setFirstName("Empty");
        this.setLastName("Empty");
        this.setBirthDate(null);
        this.setCreateDate(null);
        this.setUserType("Empty");
    }

    public User(int id, String email, String password, String firstName, String lastName, Date birthDate, Date createDate, String userType) {
        this.setId(id);
        this.setEmail(email);
        this.setPassword(password);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setBirthDate(birthDate);
        this.setCreateDate(createDate);
        this.setUserType(userType);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
