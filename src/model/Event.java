package model;

import java.sql.Date;
import java.util.List;

public class Event {

    private int id;
    private String name;
    private String desc;
    private Date date;
    private int participants;
    private int businessId;
    private List <Stage> stage;

    public Event() {
        this.setId(0);
        this.setName("Empty");
        this.setDesc("Empty");
        this.setDate(null);
        this.setParticipants(0);
        this.setBusinessId(0);
    }

    public Event(int id, String name, String desc, Date date, int participants, int businessId, List<Stage> stage) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.date = date;
        this.participants = participants;
        this.businessId = businessId;
        this.stage = stage;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDesc() { return desc; }

    public void setDesc(String desc) { this.desc = desc; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public int getParticipants() { return participants; }

    public void setParticipants(int participants) { this.participants = participants; }

    public int getBusinessId() { return businessId; }

    public void setBusinessId(int businessId) { this.businessId = businessId; }

    public List<Stage> getStage() { return stage; }

    public void setStage(List<Stage> stage) { this.stage = stage; }
}
