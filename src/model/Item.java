package model;

import java.math.BigDecimal;

public class Item {

    private int id;
    private String itemName;
    private String itemDesc;
    private String itemCat;

    // https://stackoverflow.com/questions/4826604/how-to-use-money-data-type-in-java-sql-orm
    private BigDecimal itemPrice;
    private int menuId;

    public Item() {
        this.setId(0);
        this.setItemName("");
        this.setItemDesc("");
        this.setItemCat("");
        this.setItemPrice(BigDecimal.valueOf(0.00));
        this.setMenuId(0);
    }

    public Item(int id, String name, String desc, String cat, BigDecimal price, int menuId) {
        this.setId(id);
        this.setItemName(name);
        this.setItemDesc(desc);
        this.setItemCat(cat);
        this.setItemPrice(price);
        this.setMenuId(menuId);
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getItemName() { return itemName; }

    public void setItemName(String itemName) { this.itemName = itemName; }

    public String getItemDesc() { return itemDesc; }

    public void setItemDesc(String itemDesc) { this.itemDesc = itemDesc; }

    public String getItemCat() { return itemCat; }

    public void setItemCat(String itemCat) { this.itemCat = itemCat; }

    public BigDecimal getItemPrice() { return itemPrice; }

    public void setItemPrice(BigDecimal itemPrice) { this.itemPrice = itemPrice; }

    public int getMenuId() { return menuId; }

    public void setMenuId(int menuId) { this.menuId = menuId; }
}
