package model;

import java.util.List;

public class Menu {

    private int id;
    private String menuName;
    private int businessId;
    private List<Item> item;

    public Menu() {
        this.setId(0);
        this.setMenuName("Empty");
        this.setBusinessId(0);
    }

    public Menu(int id, String name, int businessId) {
        this.setId(id);
        this.setMenuName(name);
        this.setBusinessId(businessId);
    }

    public Menu(int id, String name, int businessId, List<Item> item) {
        this.setId(id);
        this.setMenuName(name);
        this.setBusinessId(businessId);
        this.setItem(item);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public List<Item> getItem() { return item; }

    public void setItem(List<Item> item) { this.item = item; }
}
