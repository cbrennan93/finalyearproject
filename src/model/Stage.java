package model;

import java.util.List;

public class Stage {

    private int id;
    private int stageNumber;
    private int eventId;
    private int businessId;
    private Business business;

    public Stage() {
        this.setId(0);
        this.setStageNumber(0);
        this.setEventId(0);
        this.setBusinessId(0);
    }

    public Stage(int id, int stageNumber, int eventId, int businessId) {
        this.id = id;
        this.stageNumber = stageNumber;
        this.eventId = eventId;
        this.businessId = businessId;
    }

    public Stage(int id, int stageNumber, int eventId, int businessId, Business business) {
        this.id = id;
        this.stageNumber = stageNumber;
        this.eventId = eventId;
        this.businessId = businessId;
        this.business = business;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public int getStageNumber() { return stageNumber; }

    public void setStageNumber(int stageNumber) { this.stageNumber = stageNumber; }

    public int getEventId() { return eventId; }

    public void setEventId(int eventId) { this.eventId = eventId; }

    public int getBusinessId() { return businessId; }

    public void setBusinessId(int businessId) { this.businessId = businessId; }

    public Business getBusiness() { return business; }

    public void setBusiness(Business business) { this.business = business; }
}
