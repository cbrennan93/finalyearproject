package model;

public class Hour {

    private int id;
    private String day;
    private String open;
    private String close;
    private int businessId;

    public Hour() {
        this.setId(0);
        this.setDay("");
        this.setOpen("");
        this.setClose("");
        this.setBusinessId(0);
    }

    public Hour(int id, String day, String open, String close, int businessId) {
        this.id = id;
        this.day = day;
        this.open = open;
        this.close = close;
        this.businessId = businessId;
    }

    public Hour(String day, String open, String close, int businessId) {
        this.day = day;
        this.open = open;
        this.close = close;
        this.businessId = businessId;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getDay() { return day; }

    public void setDay(String day) { this.day = day; }

    public String getOpen() { return open; }

    public void setOpen(String open) { this.open = open; }

    public String getClose() { return close; }

    public void setClose(String close) { this.close = close; }

    public int getBusinessId() { return businessId; }

    public void setBusinessId(int businessId) { this.businessId = businessId; }
}
