package servlets;

import dao.UserDAO;
import model.User;
import utils.IConstants;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserServlet extends HttpServlet implements IConstants {

    // Service method directs to other methods
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {

        String appAction = request.getParameter("function");
        if (appAction != null) {
            if (appAction.equals("viewAllUsers")) {
                viewAllUsers(request, response);
            }
            else if (appAction.equals("viewDaysCount")) {
                viewDaysCount(request, response);
            }
            else if (appAction.equals("addUser")) {
                addUser(request, response);
            }
            else if (appAction.equals("viewUser")) {
                viewUser(request, response);
            }
            else if (appAction.equals("deleteUser")) {
                deleteUser(request, response);
            }
            else if (appAction.equals("updateUser")) {
                updateUser(request, response);
            }
            else if (appAction.equals("logout")) {
                logout(request, response);
            }
        }
    }

    private void viewAllUsers(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        UserDAO userDAO = new UserDAO();
        Vector<User> allUsersVector = userDAO.getAllUsers();
        request.getSession(true).setAttribute(IConstants.SESSION_KEY_ALL_USERS, allUsersVector);

        RequestDispatcher rd = request.getRequestDispatcher("/dashboard.jsp");
        rd.forward(request, response);
    }

    private void viewDaysCount(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ClassNotFoundException, ServletException, IOException {
        UserDAO userDAO = new UserDAO();
        int[] allDays = userDAO.getCreatedDays();
        request.setAttribute("alldays", allDays);
        RequestDispatcher rd = request.getRequestDispatcher("/dashboard.jsp");
        rd.forward(request, response);

        // TODO stats
        // https://stackoverflow.com/questions/25983786/how-to-pass-parameters-from-servlet-to-javascript
    }


    // Murach JSP Book and examples https://www.murach.com/shop/murach-s-java-servlets-and-jsp-3rd-edition-detail
    private void addUser(HttpServletRequest request, HttpServletResponse response)
            throws ParseException, ServletException, IOException {

        UserDAO userDAO = new UserDAO();

        // get parameters from the request
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordConf = request.getParameter("passwordConf");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String birthDate = request.getParameter("birthDate");
        String userType = request.getParameter("userType");
        String message;

        // https://stackoverflow.com/questions/27617154/how-to-insert-date-into-mysql
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date utilDate = format.parse(birthDate);
        java.sql.Date sqlDate;
        sqlDate = new java.sql.Date(utilDate.getTime());
        if (password.equals(passwordConf)) {
            // store data in User object
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setBirthDate(sqlDate);
            user.setUserType(userType);

            message = "Successfully registered";

            userDAO.addUser(user);

            request.setAttribute("message", message);
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }
        else if (!password.equals(passwordConf)) {

            message = "Passwords don't match";
            request.setAttribute("message", message);
            RequestDispatcher rd = request.getRequestDispatcher("/register.jsp");
            rd.forward(request, response);

        } else {

            message = "Error, please try again";
            request.setAttribute("message", message);
            RequestDispatcher rd = request.getRequestDispatcher("/register.jsp");
            rd.forward(request, response);

        }

    }

    private void viewUser (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        UserDAO uDAO = new UserDAO();
        int id = Integer.parseInt(request.getParameter("id"));

        User user = uDAO.getUser(id);

        request.getSession(true).setAttribute(IConstants.SESSION_KEY_USER, user);
        RequestDispatcher rd = request.getRequestDispatcher("/user-profile.jsp");
        request.setAttribute("tempUser", user);
        rd.forward(request, response);

    }

    private void deleteUser (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        UserDAO uDAO = new UserDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        uDAO.deleteUser(id);

        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);

    }

    private void updateUser (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException, ParseException {

        UserDAO uDAO = new UserDAO();
        int id = Integer.parseInt(request.getParameter("id"));

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String birthDate = request.getParameter("birthDate");

        // https://stackoverflow.com/questions/27617154/how-to-insert-date-into-mysql
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date utilDate = format.parse(birthDate);
        java.sql.Date sqlDate;
        sqlDate = new java.sql.Date(utilDate.getTime());

        if (firstName != null && !firstName.equals("") && lastName != null && !lastName.equals("") && birthDate != null && !birthDate.equals("")) {

            User user = new User();
            user.setId(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setBirthDate(sqlDate);

            int i = uDAO.updateUser(user);

            if (i > 0) {
                request.getSession(true).setAttribute(IConstants.SESSION_KEY_USER, user);
                RequestDispatcher rd = request.getRequestDispatcher("/UserManagement?function=viewUser&id=" + id);
                request.setAttribute("tempUser", user);
                rd.forward(request, response);
            } else {
                System.out.println("update failed");
            }

        } else {

            String message = "Unable to update record";
            request.setAttribute("message", message);
            RequestDispatcher rd = request.getRequestDispatcher("UserManagement?function=viewUser&id=" + id);
            rd.forward(request, response);

        }
    }

    private void logout (HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        HttpSession session = request.getSession(false);
        if(session != null) {
            session.invalidate();
            response.sendRedirect(request.getContextPath() + "/index.jsp");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ParseException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ParseException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
