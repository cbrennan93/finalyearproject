package servlets;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import dao.BusinessDAO;
import dao.EventDAO;
import manager.EventManager;
import model.*;
import utils.IConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

public class EventServlet extends HttpServlet implements IConstants {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {

        String appAction = request.getParameter("function");
        if (appAction != null) {
            switch (appAction) {
                case "viewAllEvent":
                    viewAllEvent(request, response);
                    break;
                case "viewAllLiveEvent":
                    viewAllLiveEvent(request, response);
                    break;
                case "addEvent":
                    addEvent(request, response);
                    break;
                case "viewEvent":
                    viewEvent(request, response);
                    break;
                case "viewLiveEvent":
                    viewLiveEvent(request, response);
                    break;
                case "addStage":
                    addStage(request, response);
                    break;
            }
        }

    }

    private void viewAllEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        // https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) request.getSession(false).getAttribute("SKUSER");
            if(user != null) {
                int userId = user.getId();
                EventManager eMgr = new EventManager();
                List<Business> business = eMgr.getBusinessEvent();
                Vector<Business> allBusinessVector = bDAO.getAllBusiness(userId);
                request.getSession(true).setAttribute(IConstants.SESSION_KEY_ALL_BUSINESS, allBusinessVector);
                request.setAttribute("business", business);
                RequestDispatcher rd = request.getRequestDispatcher("/event-admin.jsp");
                rd.forward(request, response);
            } else {
                request.setAttribute("errMessage", "Please log in again");
                RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
                rd.forward(request, response);
            }
        } else {
            request.setAttribute("errMessage", "Please log in again");
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }

    }

    private void viewAllLiveEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        EventManager eMgr = new EventManager();
        List<Business> business = eMgr.getBusinessEvent();
        request.setAttribute("business", business);
        RequestDispatcher rd = request.getRequestDispatcher("/events.jsp");
        rd.forward(request, response);

    }

    private void addEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        EventDAO eDAO = new EventDAO();

        String url;
        int part;

        // get parameters from the request
        String name = request.getParameter("eventName");
        String desc = request.getParameter("eventDesc");
        Date date = Date.valueOf(request.getParameter("eventDate"));
        int tempPart = Integer.parseInt(request.getParameter("eventPart"));
        int businessId = Integer.parseInt(request.getParameter("businessId"));

        // https://javapapers.com/core-java/post-to-twitter-using-java/
        String consumerKeyStr = "0UXTuLbvD0NKMIkBRD6WYJGRv";
        String consumerSecretStr = "mWuSHPmZv1ARtuC9hx4Ls5z7BF83a3cXtK4rFo0GDGSdYXHuHl";
        String accessTokenStr = "1102188956861501445-2mT5GjhEKByqCJxeI3OnUCqyBk8shv";
        String accessTokenSecretStr = "GUe6ClRpJ7cgjo89X9aUwBwB4lg8hC6hwDwMhqUao1gIg";

        BusinessDAO bDAO = new BusinessDAO();
        Business business = bDAO.getBusiness(businessId);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = formatter.format(date);
        String hashtags = "#pubcrawl #foodtrail #eventfairy";
        String tweet = "New Event: " + name + " starting in " + business.getName() + " on the " + strDate + " " + hashtags;
        System.out.println(tweet);

        try {
            Twitter twitter = new TwitterFactory().getInstance();

            twitter.setOAuthConsumer(consumerKeyStr, consumerSecretStr);
            AccessToken accessToken = new AccessToken(accessTokenStr,
                    accessTokenSecretStr);

            twitter.setOAuthAccessToken(accessToken);

            twitter.updateStatus(tweet);

            System.out.println("Successfully updated the status in Twitter.");
        } catch (TwitterException te) {
            te.printStackTrace();
        }

        if(tempPart <= 0) {
             part = 1;
        } else {
            part = tempPart;
        }

        // store data in Event object
        Event e = new Event();
        e.setName(name);
        e.setDesc(desc);
        e.setDate(date);
        e.setParticipants(part);
        e.setBusinessId(businessId);

        // validate the parameters
        String message;
        if (name == null || desc == null || date == null || businessId == 0 ) {
            message = "Please fill out all text boxes.";
            url = "/event-admin.jsp";
        }
        else {
            message = "Successfully registered";
            url = "/Event?function=viewAllEvent";
            eDAO.addEvent(e);
        }

        request.setAttribute("message", message);

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);

    }

    private void viewEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        // https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) request.getSession(false).getAttribute("SKUSER");
            if(user != null) {
                //Get and set the event and business names for the top of the item page using DAOs
                int eventId = Integer.parseInt(request.getParameter("id"));
                EventManager eMgr = new EventManager();
                Event e = eMgr.getEventById(eventId);
                //Stops null pointer if event doesn't exist
                if (e.getBusinessId() != 0) {
                    String eventName = e.getName();
                    request.setAttribute("eventName", eventName);
                    List<Stage> s = e.getStage();
                    request.setAttribute("stage", s);
                }
                RequestDispatcher rd = request.getRequestDispatcher("/stage-admin.jsp");
                rd.forward(request, response);
            } else {
                request.setAttribute("errMessage", "Please log in again");
                RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
                rd.forward(request, response);
            }
        } else {
            request.setAttribute("errMessage", "Please log in again");
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }

    }

    private void viewLiveEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        int eventId = Integer.parseInt(request.getParameter("id"));
        EventManager eMgr = new EventManager();
        Event e = eMgr.getEventById(eventId);
        //Stops null pointer if event doesn't exist
        if (e.getBusinessId() != 0) {
            request.setAttribute("event", e);
            String eventName = e.getName();
            request.setAttribute("eventName", eventName);
            List<Stage> s = e.getStage();
            request.setAttribute("stage", s);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/live-event.jsp");
        rd.forward(request, response);

    }

    private void addStage(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        int eventId;
        int eventPart;
        int businessId;

        eventId = Integer.parseInt(request.getParameter("eventId"));
        eventPart = Integer.parseInt(request.getParameter("eventPart"));
        businessId = Integer.parseInt(request.getParameter("businessId"));

        if (eventId != 0 && eventPart != 0 && businessId != 0) {

            EventManager eMgr = new EventManager();
            int i = eMgr.manageStage(eventId, eventPart, businessId);

            if (i > 0) {
                RequestDispatcher rd = request.getRequestDispatcher("/Event?function=viewAllEvent");
                rd.forward(request, response);

            } else {
                System.out.println("update failed");
            }

        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/Event?function=viewAllEvent");
            rd.forward(request, response);
        }
    }




    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
