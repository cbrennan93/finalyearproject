package servlets;

import dao.LoginDAO;
import model.User;
import utils.IConstants;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/* https://krazytech.com/programs/session-role-based-java-login-example */

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LoginServlet() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");


        LoginDAO loginDao = new LoginDAO();

        try
        {
            User userValidate = loginDao.authenticateUser(email, password);

            if (userValidate == null) {
                System.out.println("Error message = " + "Invalid credentials");
                request.setAttribute("errMessage", "Invalid credentials");
                request.getRequestDispatcher("/signin.jsp").forward(request, response);
            } else {

                if (userValidate.getUserType().equals(IConstants.USER_TYPE_ADMIN)) {
                    System.out.println("Admin's Home");
                    request.getSession(true).setAttribute(IConstants.SESSION_KEY_USER, userValidate);
                    request.getRequestDispatcher("/UserManagement?function=viewAllUsers").forward(request, response);

                } else if (userValidate.getUserType().equals(IConstants.USER_TYPE_GENERAL_USER)) {
                    System.out.println("Editor's Home");
                    request.getSession(true).setAttribute(IConstants.SESSION_KEY_USER, userValidate);
                    request.getRequestDispatcher("/user.jsp").forward(request, response);

                } else if (userValidate.getUserType().equals(IConstants.USER_TYPE_REGISTERED_USER)) {
                    System.out.println("User's Home");
                    request.getSession(true).setAttribute(IConstants.SESSION_KEY_USER, userValidate);
                    request.getRequestDispatcher("/index.jsp").forward(request, response);

                } else {
                    System.out.println("Error message = " + "Invalid credentials");
                    request.setAttribute("errMessage", "Invalid credentials");
                    request.getRequestDispatcher("/signin.jsp").forward(request, response);
                }
            }
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
        catch (Exception e2)
        {
            e2.printStackTrace();
        }
    } //End of doPost()

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
