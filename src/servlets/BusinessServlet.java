package servlets;

import com.google.gson.Gson;
import dao.BusinessDAO;
import dao.HourDAO;
import dao.ItemDAO;
import dao.MenuDAO;
import manager.BusinessManager;
import model.*;
import utils.IConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;

public class BusinessServlet extends HttpServlet implements IConstants {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {

        String appAction = request.getParameter("function");
        if (appAction != null) {
            switch (appAction) {
                case "viewAllBusiness":
                    viewAllBusiness(request, response);
                    break;
                case "addBusiness":
                    addBusiness(request, response);
                    break;
                case "viewBusiness":
                    viewBusiness(request, response);
                    break;
                case "updateBusiness":
                    updateBusiness(request, response);
                    break;
                case "deleteBusiness":
                    deleteBusiness(request, response);
                    break;
                case "viewIndex":
                    viewIndex(request, response);
                    break;
                case "viewAllMenu":
                    viewAllMenu(request, response);
                    break;
                case "addMenu":
                    addMenu(request, response);
                    break;
                case "editMenu":
                    editMenu(request, response);
                    break;
                case "updateMenu":
                    updateMenu(request, response);
                    break;
                case "deleteMenu":
                    deleteMenu(request, response);
                    break;
                case "addItem":
                    addItem(request, response);
                    break;
                case "updateItem":
                    updateItem(request, response);
                    break;
                case "deleteItem":
                    deleteItem(request, response);
                    break;
                case "viewAllHour":
                    viewAllHour(request, response);
                    break;
                case "addHours":
                    addHour(request, response);
                    break;
                case "viewHours":
                    viewHours(request, response);
                    break;
            }
        }
    }

    private void viewAllBusiness(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        // https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) request.getSession(false).getAttribute("SKUSER");
            if(user != null) {
                int userId = user.getId();
                Vector<Business> allBusinessVector = bDAO.getAllBusiness(userId);
                request.getSession(true).setAttribute(IConstants.SESSION_KEY_ALL_BUSINESS, allBusinessVector);
                RequestDispatcher rd = request.getRequestDispatcher("/business-admin.jsp");
                rd.forward(request, response);
            } else {
                request.setAttribute("errMessage", "Please log in again");
                RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
                rd.forward(request, response);
            }
        } else {
            request.setAttribute("errMessage", "Please log in again");
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }

    }

    private void addBusiness(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BusinessDAO bDAO = new BusinessDAO();

        String url;

        // get parameters from the request
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        int userId = Integer.parseInt(request.getParameter("userId"));
        String placeIdMaps = request.getParameter("placeIdMaps");
        String tempLat = request.getParameter("lat");
        String tempLng = request.getParameter("long");
        BigDecimal lat = new BigDecimal(tempLat);
        BigDecimal lng = new BigDecimal(tempLng);


        // store data in Business object
        Business b = new Business();
        b.setName(name);
        b.setAddress(address);
        b.setLat(lat);
        b.setLng(lng);
        b.setPlaceIdMaps(placeIdMaps);
        b.setUserId(userId);

        // validate the parameters
        String message;
        if (name == null || address == null || userId == 0 ) {
            message = "Please fill out all text boxes.";
            url = "/business-admin.jsp";
        }
        else {
            message = "Successfully registered";
            url = "/Business?function=viewAllBusiness";
            bDAO.addBusiness(b);
        }

        request.setAttribute("message", message);

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);

    }

    private void viewBusiness(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        int id = Integer.parseInt(request.getParameter("id"));
        BusinessManager bMgr = new BusinessManager();
        Business business = bMgr.getBusiness(id);
        List<Hour> hours = business.getHours();
        Gson gson = new Gson();
        String b = gson.toJson(business);
        request.getSession(true).setAttribute(IConstants.SESSION_KEY_BUSINESS, business);
        RequestDispatcher rd = request.getRequestDispatcher("/home.jsp");
        request.setAttribute("business", business);
        request.setAttribute("hours", hours);
        request.setAttribute("b", b);
        rd.forward(request, response);

    }

    private void updateBusiness(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();

        // get parameters from the request
        String name = request.getParameter("updateBusinessName");
        String address = request.getParameter("updateBusinessAddress");
        String tempLat = request.getParameter("updateLat");
        String tempLong = request.getParameter("updateLong");
        String placeIdMaps = request.getParameter("updatePlaceIdMaps");
        int id = Integer.parseInt(request.getParameter("updateBusinessId"));
        BigDecimal lat = new BigDecimal(tempLat);
        BigDecimal lng = new BigDecimal(tempLong);

        if (name != null && !name.equals("") && address != null && !address.equals("")) {

            Business b = new Business();
            b.setId(id);
            b.setName(name);
            b.setAddress(address);
            b.setLat(lat);
            b.setLng(lng);
            b.setPlaceIdMaps(placeIdMaps);

            int i = bDAO.updateBusiness(b);

            if (i > 0) {
                RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllBusiness");
                rd.forward(request, response);

            } else {
                System.out.println("update failed");
            }

        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllBusiness");
            rd.forward(request, response);
        }
    }

    private void deleteBusiness(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        bDAO.deleteBusiness(id);

        RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllBusiness");
        rd.forward(request, response);

    }

    private void viewIndex(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        int tempId = 0;
        Vector<Business> allBusinessVector = bDAO.getAllBusiness(tempId);
        // https://stackoverflow.com/questions/5805602/how-to-sort-list-of-objects-by-some-property
        allBusinessVector.sort(Comparator.comparing(Business::getName));
//        request.getSession(true).setAttribute(IConstants.SESSION_KEY_ALL_BUSINESS_INDEX, allBusinessVector);
        String json = new Gson().toJson(allBusinessVector);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
//        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
//        rd.forward(request, response);

    }


    private void viewAllMenu(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        BusinessDAO bDAO = new BusinessDAO();
        // https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) request.getSession(false).getAttribute("SKUSER");
            if(user != null) {
                int userId = user.getId();
                BusinessManager bMgr = new BusinessManager();
                List<Business> business = bMgr.getBusinessMenu(userId);
                Vector<Business> allBusinessVector = bDAO.getAllBusiness(userId);
                request.getSession(true).setAttribute(IConstants.SESSION_KEY_ALL_BUSINESS, allBusinessVector);
                request.setAttribute("business", business);
                RequestDispatcher rd = request.getRequestDispatcher("/menu-admin.jsp");
                rd.forward(request, response);
            } else {
                request.setAttribute("errMessage", "Please log in again");
                RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
                rd.forward(request, response);
            }
        } else {
            request.setAttribute("errMessage", "Please log in again");
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }

    }

    private void addMenu (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//TODO order of menus
        MenuDAO mDAO = new MenuDAO();

        String url;

        // get parameters from the request
        String name = request.getParameter("menuName");
        int businessId  = Integer.parseInt(request.getParameter("businessId"));

        // store data in Menu object
        Menu m = new Menu();
        m.setMenuName(name);
        m.setBusinessId(businessId);

        // validate the parameters
        String message;
        if (name == null || businessId == 0) {
            message = "Please fill out all text boxes.";
            url = "/menu-admin.jsp";
        }
        else {
            message = "Successfully registered";
            url = "/Business?function=viewAllMenu";
            mDAO.addMenu(m);
        }

        request.setAttribute("message", message);

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);

    }

    private void updateMenu (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        MenuDAO mDAO = new MenuDAO();

        // get parameters from the request
        int menuId = Integer.parseInt(request.getParameter("updateMenuId"));
        String name = request.getParameter("updateMenuName");
        int businessId;
        businessId = Integer.parseInt(request.getParameter("updateBusinessId"));

        if (name != null && !name.equals("") && businessId != 0) {

            Menu m = new Menu();
            m.setId(menuId);
            m.setMenuName(name);
            m.setBusinessId(businessId);

            int i = mDAO.updateMenu(m);

            if (i > 0) {
                RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllMenu");
                rd.forward(request, response);

            } else {
                System.out.println("update failed");
            }

        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllMenu");
            rd.forward(request, response);
        }

    }

    private void editMenu (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        // https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) request.getSession(false).getAttribute("SKUSER");
            if(user != null) {
                //Get and set the menu and business names for the top of the item page using DAOs
                int menuId = Integer.parseInt(request.getParameter("id"));
                MenuDAO mDAO = new MenuDAO();
                Menu m = mDAO.getMenuById(menuId);
                //Stops null pointer if menu doesn't exist
                if (m.getBusinessId() != 0) {
                    int businessId = m.getBusinessId();
                    String menuName = m.getMenuName();
                    request.setAttribute("menuName", menuName);
                    request.setAttribute("addItemMenuId", menuId);
                    BusinessDAO bDAO = new BusinessDAO();
                    Business b = bDAO.getBusiness(businessId);
                    String businessName = b.getName();
                    request.setAttribute("businessName", businessName);
                    ItemDAO iDAO = new ItemDAO();
                    List<Item> i = iDAO.getItem(menuId);
                    request.setAttribute("items", i);
                }
                RequestDispatcher rd = request.getRequestDispatcher("/item-admin.jsp");
                rd.forward(request, response);
            } else {
                    request.setAttribute("errMessage", "Please log in again");
                    RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
                    rd.forward(request, response);
            }
        } else {
            request.setAttribute("errMessage", "Please log in again");
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }
    }

    private void deleteMenu (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        MenuDAO mDAO = new MenuDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        mDAO.deleteMenu(id);

        RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllMenu");
        rd.forward(request, response);

    }

    private void addItem (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//TODO order of items
        ItemDAO iDAO = new ItemDAO();

        String url;

        // get parameters from the request
        int menuId  = Integer.parseInt(request.getParameter("menuId"));
        String name = request.getParameter("itemName");
        String desc = request.getParameter("itemDesc");
        String cat = request.getParameter("itemCat");
        String tempPrice  = request.getParameter("itemPrice");
        BigDecimal price = new BigDecimal(tempPrice);

        // store data in Item object
        Item i = new Item();
        i.setItemName(name);
        i.setItemDesc(desc);
        i.setItemCat(cat);
        i.setItemPrice(price);
        i.setMenuId(menuId);

        // validate the parameters
        String message;
        if (name == null || desc == null || cat == null || price.compareTo(BigDecimal.ZERO) == 0 || menuId == 0) {
            message = "Please fill out all text boxes.";
            url = "/Business?function=editMenu&id=" + menuId;
        }
        else {
            message = "Successfully registered";
            url = "/Business?function=editMenu&id=" + menuId;
            iDAO.addItem(i);
        }

        request.setAttribute("message", message);

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);

    }

    private void updateItem(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        ItemDAO iDAO = new ItemDAO();

        // get parameters from the request
        int itemId = Integer.parseInt(request.getParameter("updateItemId"));
        String name = request.getParameter("updateItemName");
        String desc = request.getParameter("updateItemDesc");
        String cat = request.getParameter("updateItemCat");
        String tempPrice = request.getParameter("updateItemPrice");
        BigDecimal price = new BigDecimal(tempPrice);
        int menuId = Integer.parseInt(request.getParameter("updateMenuId"));

        // https://stackoverflow.com/questions/15936286/validating-bigdecimal-data-type-for-nulls-in-java
        if (name != null && !name.equals("") && desc != null && !desc.equals("") && cat != null && !cat.equals("") && price.compareTo(BigDecimal.ZERO) != 0 && menuId != 0) {

            Item item = new Item();
            item.setId(itemId);
            item.setItemName(name);
            item.setItemDesc(desc);
            item.setItemCat(cat);
            item.setItemPrice(price);

            int i = iDAO.updateItem(item);

            if (i > 0) {
                RequestDispatcher rd = request.getRequestDispatcher("/Business?function=editMenu&id=" + menuId);
                rd.forward(request, response);

            } else {
                System.out.println("update failed");
            }

        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllMenu");
            rd.forward(request, response);
        }
    }

    private void deleteItem(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        ItemDAO iDAO = new ItemDAO();
        int id = Integer.parseInt(request.getParameter("id"));
        int menuId = Integer.parseInt(request.getParameter("menuId"));
        iDAO.deleteItem(id);

        RequestDispatcher rd = request.getRequestDispatcher("/Business?function=editMenu&id=" + menuId);
        rd.forward(request, response);

    }

    private void viewAllHour(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {


        BusinessDAO bDAO = new BusinessDAO();
        // https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) request.getSession(false).getAttribute("SKUSER");
            if(user != null) {
                int userId = user.getId();
                BusinessManager bMgr = new BusinessManager();
                List<Business> business = bMgr.getBusinessHour(userId);
                Vector<Business> allBusinessVector = bDAO.getAllBusiness(userId);
                request.getSession(true).setAttribute(IConstants.SESSION_KEY_ALL_BUSINESS, allBusinessVector);
                request.setAttribute("business", business);
                RequestDispatcher rd = request.getRequestDispatcher("/hours-admin.jsp");
                rd.forward(request, response);
            } else {
                request.setAttribute("errMessage", "Please log in again");
                RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
                rd.forward(request, response);
            }
        } else {
            request.setAttribute("errMessage", "Please log in again");
            RequestDispatcher rd = request.getRequestDispatcher("/signin.jsp");
            rd.forward(request, response);
        }

    }

    private void addHour(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {

        int businessId = Integer.parseInt(request.getParameter("businessId"));

        String mon = "Monday";
        String tues = "Tuesday";
        String wed = "Wednesday";
        String thurs = "Thursday";
        String fri = "Friday";
        String sat = "Saturday";
        String sun = "Sunday";

        String monOpen = request.getParameter("monOpen");
        String tuesOpen = request.getParameter("tuesOpen");
        String wedOpen = request.getParameter("wedOpen");
        String thursOpen = request.getParameter("thursOpen");
        String friOpen = request.getParameter("friOpen");
        String satOpen = request.getParameter("satOpen");
        String sunOpen = request.getParameter("sunOpen");

        String monClose = request.getParameter("monClose");
        String tuesClose = request.getParameter("tuesClose");
        String wedClose = request.getParameter("wedClose");
        String thursClose = request.getParameter("thursClose");
        String friClose = request.getParameter("friClose");
        String satClose = request.getParameter("satClose");
        String sunClose = request.getParameter("sunClose");

        Hour hourMon = new Hour(mon, monOpen, monClose, businessId);
        Hour hourTues = new Hour(tues, tuesOpen, tuesClose, businessId);
        Hour hourWed = new Hour(wed, wedOpen, wedClose, businessId);
        Hour hourThurs = new Hour(thurs, thursOpen, thursClose, businessId);
        Hour hourFri = new Hour(fri, friOpen, friClose, businessId);
        Hour hourSat = new Hour(sat, satOpen, satClose, businessId);
        Hour hourSun = new Hour(sun, sunOpen, sunClose, businessId);

        List<Hour> hourList = new ArrayList<>();
        hourList.add(hourMon);
        hourList.add(hourTues);
        hourList.add(hourWed);
        hourList.add(hourThurs);
        hourList.add(hourFri);
        hourList.add(hourSat);
        hourList.add(hourSun);

        HourDAO hDAO = new HourDAO();
        int i = hDAO.addHours(hourList, businessId);

        if (i > 0) {
            RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllHour");
            rd.forward(request, response);

        } else {
            request.setAttribute("message", "Could not add, please contact Admin");
            RequestDispatcher rd = request.getRequestDispatcher("/Business?function=viewAllHour");
            rd.forward(request, response);
        }


    }

    private void viewHours(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException, ClassNotFoundException {

        // https://stackoverflow.com/questions/4112686/how-to-use-servlets-and-ajax
        int businessId = Integer.parseInt(request.getParameter("businessId"));

        BusinessManager bMgr = new BusinessManager();
        Business business = bMgr.getBusiness(businessId);
        request.setAttribute("business", business);

        String json = new Gson().toJson(business);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);


    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
