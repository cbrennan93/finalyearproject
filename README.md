# README #

This README would normally document whatever steps are necessary to get your application up and running.

##List of code from other sources

###Database connection
* DAO, DbManager, SetupDb, DbInitServlet
    * https://www.youtube.com/watch?v=CHBfplHDO7I
    * https://stackoverflow.com/questions/34189756/warning-about-ssl-connection-when-connecting-to-mysql-database
    *  3rd year java project
* IConstants
    * 3rd year java project
    
###DAOs
* UserDAO
    * addUser()
        * https://www.codejava.net/coding/jsp-servlet-jdbc-mysql-create-read-update-delete-crud-example
        * https://stackoverflow.com/questions/27177494/how-to-insert-date-into-database-using-jsp
        * https://stackoverflow.com/questions/13112811/how-to-insert-current-date-into-mysql-db-using-java
    * updateUser()
        * https://coderanch.com/t/304094/databases/return-type-executeUpdate
* BusinessDAO
    * getBusiness()
        * https://stackoverflow.com/questions/7363930/how-to-select-a-single-record-in-a-left-join
    * getAllBusiness()
        * https://stackoverflow.com/questions/22604952/error-data-source-rejected-establishment-of-connection-message-from-server-t
* EventDAO
    * getEvent()
        * https://stackoverflow.com/questions/5927109/sort-objects-in-arraylist-by-date
    
###Servlets
* Login Servlet
    * https://krazytech.com/programs/session-role-based-java-login-example
* User Management Servlet
    * addUser()
        * Murach JSP Book and examples -> download here: https://www.murach.com/shop/murach-s-java-servlets-and-jsp-3rd-edition-detail
        * https://stackoverflow.com/questions/27617154/how-to-insert-date-into-mysql
* Business Management Servlet
    * viewAllBusiness()
        * https://stackoverflow.com/questions/2818251/how-to-check-if-session-exists-or-not
    * addItem()
        * https://stackoverflow.com/questions/15936286/validating-bigdecimal-data-type-for-nulls-in-java
    * viewIndex()
        * https://stackoverflow.com/questions/5805602/how-to-sort-list-of-objects-by-some-property
    * viewHours()
        *  // https://stackoverflow.com/questions/4112686/how-to-use-servlets-and-ajax
* Event Servlet
    * addEvent()
        * https://javapapers.com/core-java/post-to-twitter-using-java/    
        
###Manager
* Business Manager
    * getBusinessMenu()
        * https://www.geeksforgeeks.org/vector-vs-arraylist-java/
    * getBusiness()
        * https://stackoverflow.com/questions/11177348/how-to-add-element-in-list-while-iterating-in-java
   
###Model
* Item Class
    * Currency handling
        * https://stackoverflow.com/questions/4826604/how-to-use-money-data-type-in-java-sql-orm
        
###JSP pages
* Index
    * https://getbootstrap.com/docs/4.1/examples/cover/
    * https://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_eff_animate_smoothscroll
    * https://stackoverflow.com/questions/15839854/how-load-servlet-on-index-jsp
* Home
    * https://getbootstrap.com/docs/4.1/examples/cover/
    * https://stackoverflow.com/questions/1000876/how-to-select-the-first-element-of-a-set-with-jstl
    * https://stackoverflow.com/questions/2017753/jstl-iterate-list-but-treat-first-element-differently
* Sign in
    * https://getbootstrap.com/docs/4.1/examples/sign-in/
    * https://krazytech.com/programs/session-role-based-java-login-example
* Register
    * https://getbootstrap.com/docs/4.1/examples/sign-in/
* Events
    * https://getbootstrap.com/docs/4.1/examples/cover/
    * https://www.codeply.com/go/jbcgzs2Nzq/bootstrap-4-equal-height-cards
    * https://www.tutorialspoint.com/jsp/jstl_format_formatdate_tag.htm
* Live Event
    * https://getbootstrap.com/docs/4.1/examples/cover/
* Dashboard
    * All Pages in Dashboard
        * https://getbootstrap.com/docs/4.1/examples/dashboard/
    * Business Admin
        * https://www.w3schools.com/bootstrap4/bootstrap_modal.asp
        * https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link
    * User Profile
        * https://stackoverflow.com/questions/547821/two-submit-buttons-in-one-form
        * https://stackoverflow.com/questions/8299391/confirmation-delete-in-jsp
        * https://stackoverflow.com/questions/4587397/how-to-use-if-else-option-in-jstl
    * Menu
        * http://www.informit.com/articles/article.aspx?p=24123&seqNum=4

###Javascript
* Poppers
    * https://popper.js.org/ (MIT Licence) 
* JQuery
    * home.jsp
        * https://stackoverflow.com/questions/18055524/show-one-div-and-hide-others-on-clicking-a-link
        * https://www.w3schools.com/jquery/jquery_events.asp
        * https://www.w3schools.com/jquery/jquery_css_classes.asp
        * https://stackoverflow.com/questions/4667902/how-to-use-jquery-to-find-a-link-within-the-div-with-a-certain-class-and-make-it
    * menu-admin.jsp
        * https://www.daftlogic.com/information-programmatically-preselect-dropdown-using-javascript.htm
    * business-admin.jsp
        * https://stackoverflow.com/questions/7484057/jquery-how-to-write-text-values-to-a-hidden-field-for-later-use
        * https://stackoverflow.com/questions/15730913/concatenate-multiple-html-text-inputs-with-stored-variable
        * https://stackoverflow.com/questions/14042193/how-to-trigger-an-event-in-input-text-after-i-stop-typing-writing
        * https://stackoverflow.com/questions/11456862/get-a-json-file-from-url-and-display
    * event-admin.jsp
        * https://stackoverflow.com/questions/32378590/set-date-input-fields-max-date-to-today
    * stage-admin.jsp
        * https://stackoverflow.com/questions/1149958/jquery-count-number-of-rows-in-a-table
        * https://stackoverflow.com/questions/6593473/lat-long-array-as-markers-on-google-maps-api-v3
        * https://stackoverflow.com/questions/46935093/google-maps-error-not-a-latlng-or-latlngliteral-invalidvalueerror
    * live-events.jsp
        * https://stackoverflow.com/questions/44902684/jsp-list-populate-into-javascript-array
        * https://stackoverflow.com/questions/6593473/lat-long-array-as-markers-on-google-maps-api-v3
        * https://stackoverflow.com/questions/46935093/google-maps-error-not-a-latlng-or-latlngliteral-invalidvalueerror
        * https://stackoverflow.com/questions/41374432/how-to-disable-the-satellite-and-street-view-options-in-the-google-map
        * https://stackoverflow.com/questions/25638834/mutable-variable-is-accessible-from-closure/25638959  
###CSS
* main.css
    * https://www.w3schools.com/howto/howto_css_parallax.asp
    * https://github.com/IanLunn/Hover

###GSON
* Download and connect to project
    * https://stackoverflow.com/questions/1051640/correct-way-to-add-external-jars-lib-jar-to-an-intellij-idea-project
    
###MySQL
* Check Cascade
    * https://stackoverflow.com/questions/13134913/figure-out-if-a-table-has-a-delete-on-cascade
* Joins
    * https://stackoverflow.com/questions/7363930/how-to-select-a-single-record-in-a-left-join
* Add Column
    * http://www.mysqltutorial.org/mysql-add-column/
* Latitude Longitude
    * https://stackoverflow.com/questions/12504208/what-mysql-data-type-should-be-used-for-latitude-longitude-with-8-decimal-places
    
###APIS
* Google Maps 
    * Places Search Box
        * https://developers.google.com/maps/documentation/javascript/examples/place-search
    * Distance Matrix
        * https://developers.google.com/maps/documentation/javascript/distancematrix
    * Waypoints
        * https://developers.google.com/maps/documentation/javascript/examples/directions-waypoints
    * Marker Labels
        * https://developers.google.com/maps/documentation/javascript/examples/marker-labels

###Fonts
* Google Fonts
    * https://fonts.google.com/specimen/Roboto
    * https://fonts.google.com/specimen/Slabo+27px
    