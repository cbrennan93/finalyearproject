<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 01/11/2018
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>

<!-- CODE SOURCES
https://getbootstrap.com/docs/4.1/examples/sign-in/
https://krazytech.com/programs/session-role-based-java-login-example
-->


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicons/favicon.ico">

    <title>My FYP</title>

    <!-- Bootstrap core CSS -->
    <link href="../styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../styles/signin.css" rel="stylesheet">
    <link href="../styles/main.css" rel="stylesheet">
</head>

<body class="home-container text-center">
<form action="Login" method="POST" class="form-signin">
    <img class="mb-4" src="../img/logowhite.png" alt="" width="72" height="72">
    <h1 class="h3 mb-3 text-light font-weight-normal">Please sign in</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
    <span style="color:red"><%=(request.getAttribute("errMessage") == null) ? "" : request.getAttribute("errMessage")%></span>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <br>
    <a href="register.jsp">Create an account?</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
</form>
</body>
</html>
