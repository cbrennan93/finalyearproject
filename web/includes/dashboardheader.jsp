
<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->
<html>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicons/favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../styles/dashboard.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Admin Dashboard</a>
    <input class="form-control form-control-dark w-100" type="text">
    <%--placeholder="Search" aria-label="Search"--%>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <%-- validate user from session, output username if true, output guest if false --%>
            <% User user = (User)session.getAttribute("SKUSER"); %>
            <a class="nav-link" href="<%  if (user!= null){
                out.println("UserManagement?function=viewUser&id=" + user.getId()); }
                else { out.println("/index.jsp");} %>">
                <%  if (user!= null){
                out.println("Welcome " + user.getFirstName()); }
                else { out.println("Guest Account");} %>
            </a>
        </li>
    </ul>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="UserManagement?function=logout">Sign out</a>
        </li>
    </ul>
</nav>