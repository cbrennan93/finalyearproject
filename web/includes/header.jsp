<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 19/11/2018
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.Business" %>
<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->
<html>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicons/favicon.ico">

    <title>Homepage Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../styles/dashboard.css" rel="stylesheet">
    <link href="../styles/cover.css" rel="stylesheet">
    <link href="../styles/main.css" rel="stylesheet">

    <!-- Custom fonts -->
    <!--
        https://fonts.google.com/specimen/Slabo+27
        https://fonts.google.com/specimen/Roboto
    -->
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

