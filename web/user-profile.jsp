<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 23/01/2019
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/includes/dashboardheader.jsp"%>
<% Vector<User> allUsers = (Vector<User>) session.getAttribute("SKALLUSERS");%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="users"></span>
                            Users
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="bar-chart-2"></span>
                            Reports
                        </a>
                    </li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Saved reports</span>
                    <a class="d-flex align-items-center text-muted" href="#">
                        <span data-feather="plus-circle"></span>
                    </a>
                </h6>
                <ul class="nav flex-column mb-2">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Current month
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Last quarter
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Social engagement
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Year-end sale
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">User Profile</h1>
                <div class="btn-toolbar mb-2 mb-md-0">


                </div>
            </div>
            <div class="container">
                <form class="form-signin" action="UserManagement?function=updateUser&id=<c:out value="${tempUser.id}"/>" method="post">
                    <div class="col-12 col-md-6">
                        <input type="hidden" name="action" value="update">
                        <label for="inputFirstName" class="sr-only">First Name</label>
                        <input type="text" id="inputFirstName" name="firstName" class="form-control" placeholder="First Name" value="<c:out value='${tempUser.firstName}' />" required autofocus>
                        <br>
                        <label for="inputLastName" class="sr-only">First Name</label>
                        <input type="text" id="inputLastName" name="lastName" class="form-control" placeholder="Last Name" value="<c:out value='${tempUser.lastName}' />" required>
                        <br>
                        <label for="inputEmailReg" class="sr-only">Email address</label>
                        <input disabled type="email" id="inputEmailReg" name="email" class="form-control" placeholder="Email Address" value="<c:out value='${tempUser.email}' />" required>
                        <br>
                        <label for="inputDate" class="sr-only">Date of Birth</label>
                        <input type="date" id="inputDate" name="birthDate" class="form-control" placeholder="Date of Birth" value="<c:out value='${tempUser.birthDate}' />"required>
                        <br>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <label class="input-group-text" id="userTypeLabel" for="inputUserType">User Type</label>
                            </div>
                            <select disabled class="custom-select" id="inputUserType" name="userType" required>
                                <%-- https://stackoverflow.com/questions/4587397/how-to-use-if-else-option-in-jstl --%>
                                <c:set var="userType" scope="session" value="${tempUser.userType}"/>
                                <c:choose>
                                    <c:when test="${userType == 'Admin'}">
                                        <option value="<c:out value='${tempUser.userType}' />">${tempUser.userType}</option>
                                    </c:when>
                                    <c:when test="${userType == 'RegUser'}">
                                        <option value="<c:out value='${tempUser.userType}' />">${tempUser.userType}</option>
                                    </c:when>
                                    <c:otherwise>
                                <option value=""></option>
                                    </c:otherwise>
                                </c:choose>

                            </select>
                        </div>
                        <br>
                        <label for="inputPasswordReg" class="sr-only">Password</label>
                        <input disabled type="password" id="inputPasswordReg" name="password" class="form-control" placeholder="Password" value="<c:out value='${tempUser.password}' />" required>
                        <br>
                        <label for="inputConfirmPassword" class="sr-only">Confirm Password</label>
                        <input disabled type="password" id="inputConfirmPassword" name="passwordConf" class="form-control" placeholder="Confirm Password" value="<c:out value='${tempUser.password}' />" required>
                        <p><i>${message}</i></p>
                        <!-- https://stackoverflow.com/questions/547821/two-submit-buttons-in-one-form
                             https://stackoverflow.com/questions/8299391/confirmation-delete-in-jsp
                        -->
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <button class="btn btn-lg btn-info btn-block" name="update" type="submit">Update Profile</button>
                            </div>
                            <div class="col-12 col-md-6">
                                <button class="btn btn-lg btn-danger btn-block" name="delete" type="submit"
                                        onclick="if (confirm('Are you sure you want to delete? This cannot be undone.'))
                                        { form.action='UserManagement?function=deleteUser&id=<c:out value="${tempUser.id}"/>'; }
                                        else { return false; }">
                                    Delete Profile</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </div>
</div>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>
<%@ include file="/includes/dashboardfooter.html"%>
