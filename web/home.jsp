<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 05/03/2019
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>

<%-- https://getbootstrap.com/docs/4.1/examples/cover/ --%>

<%@ page import="model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/includes/header.jsp"%>

<body class="text-center">
<div class="home-container">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <header class="masthead mb-auto">
            <div class="inner">
                <h3 class="masthead-brand">Sláinte</h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link active" href="index.jsp">Home</a>
                    <a class="nav-link" href="Event?function=viewAllLiveEvent">Events</a>
                    <a class="nav-link" href="signin.jsp">Sign In</a>
                    <% User user = (User)session.getAttribute("SKUSER"); %>
                    <a class="nav-link" href="
                    <% if (user!= null){
                        out.println("UserManagement?function=viewUser&id=" + user.getId());
                    } else {
                        out.println("/index.jsp");
                    } %>">
                        <% if (user!= null){
                            out.println("Welcome " + user.getFirstName());
                        } else { out.println("Guest Account");
                        } %>
                    </a>
                </nav>
            </div>
        </header>
        <div class="landing">
            <main role="main" class="inner cover centering">
                <h1 class="cover-heading"><c:out value="${business.name}"/></h1>
                <p class="lead"><c:out value="${business.address}"/></p>
                <a href="#section2"><p class="lead"><span style="height: 40px; width: 40px" data-feather="chevrons-down"></span></p></a>
            </main>
        </div>
    </div>
</div>
<div id="section2" class="menu-container" style="min-height: 100%">
    <div class="container" id="nav-links">
        <div class="d-flex text-padding-top menu-background row">
            <h1 class="cover-heading">Our Menus</h1>
        </div>
        <div class="row d-flex justify-content-center menu-background pb-5">
            <c:forEach items="${business.menu}" var="menu" varStatus="menuStatus">
                <c:if test="${menuStatus.first}">
                    <a class="lead menu-border-active" rel="menu-<c:out value='${menu.id}'/>"><b><c:out value="${menu.menuName}"/></b></a>
                </c:if>
                <c:if test="${!menuStatus.first}">
                    <a class="lead menu-border" rel="menu-<c:out value='${menu.id}'/>"><b><c:out value="${menu.menuName}"/></b></a>
                </c:if>
            </c:forEach>
        </div>
        <%-- https://stackoverflow.com/questions/2017753/jstl-iterate-list-but-treat-first-element-differently --%>
        <c:forEach items="${business.menu}" var="menu" varStatus="itemStatus">
            <c:if test="${itemStatus.first}">
                <div class="row menu-background" id="menu-<c:out value='${menu.id}'/>">
                    <c:forEach items="${menu.item}" var="item">
                        <div class="col-md-5 text-left pl-5">
                            <div class="row pb-0">
                                <h6>${item.itemName}</h6>
                            </div>
                            <div class="row pt-0">
                                <p><i>${item.itemDesc}</i></p>
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <h6>&euro;${item.itemPrice}</h6>
                        </div>
                    </c:forEach>
                </div>
            </c:if>
            <c:if test="${!itemStatus.first}">
                <div class="row menu-background" id="menu-<c:out value='${menu.id}'/>" style="display: none">
                    <c:forEach items="${menu.item}" var="item">
                        <div class="col-md-5 text-left pl-5">
                            <div class="row pb-0">
                                <h6>${item.itemName}</h6>
                            </div>
                            <div class="row pt-0">
                                <p><i>${item.itemDesc}</i></p>
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <h6>&euro;${item.itemPrice}</h6>
                        </div>
                    </c:forEach>
                </div>
            </c:if>
        </c:forEach>
    </div>
</div>
<div id="section3" class="details-container" style="min-height: 100%">
    <div class="container">
        <div class="d-flex text-padding-top menu-background row">
            <h1 class="cover-heading">Our Details</h1>
        </div>
        <div class="row d-flex justify-content-center menu-background pb-5">
            <div class="col-lg-6 p-5">
                <h4 class="cover-heading">Opening Hours</h4>
                <table class="table table-borderless event-list-table" id="myTable">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Day</th>
                        <th scope="col">Open</th>
                        <th scope="col">Close</th>
                    </tr>
                    </thead>
                    <tbody class="event-list-table">
                    <c:forEach items="${hours}" var="hours">
                        <tr>
                            <td class="align-middle">${hours.day}</td>
                            <td class="align-middle">${hours.open}</td>
                            <td class="align-middle">${hours.close}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-6 p-5">
                <h4 class="cover-heading">Location</h4>
                <div class="iframe-container">
                    <iframe
                            width="500"
                            height="400"
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s&q=place_id:<c:out value="${business.placeIdMaps}"/>" allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>

<script>
    feather.replace();
    /*
    https://stackoverflow.com/questions/18055524/show-one-div-and-hide-others-on-clicking-a-link
    https://www.w3schools.com/jquery/jquery_events.asp
    https://www.w3schools.com/jquery/jquery_css_classes.asp
    https://stackoverflow.com/questions/4667902/how-to-use-jquery-to-find-a-link-within-the-div-with-a-certain-class-and-make-it
     */
    $(document).ready(function() {
        $('.menu-container a').on({
            click: function() {
                $(this).siblings("a").removeClass("menu-border-active");
                $(this).siblings("a").addClass("menu-border");
                $(this).removeClass("menu-border");
                $(this).addClass("menu-border-active");
                var target = $(this).attr('rel');
                $("#" + target).show().siblings("div").hide();
            },
            mouseenter: function() {
                $(this).siblings("a").removeClass("menu-border-active");
                $(this).siblings("a").addClass("menu-border");
                $(this).removeClass("menu-border");
                $(this).addClass("menu-border-active");
                var target = $(this).attr('rel');
                $("#" + target).show().siblings("div").hide();
            }
        });
        // https://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_eff_animate_smoothscroll
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });

</script>

<!--
TODO look at viability of adverts using Google Ads
TODO be careful about GDPR and how it pertains to project
TODO do you need to be 18 to use the app?
TODO event location in GPS
TODO event tags
TODO Each event has a stage, an order, a name of business and if it is published
TODO calendar plugin
TODO sort event by date
TODO Google analytics/twitter - promotion aspect
-->
<%@ include file="/includes/footer.jsp"%>

