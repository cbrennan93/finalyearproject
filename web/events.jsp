<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 27/02/2019
  Time: 11:57
  To change this template use File | Settings | File Templates.
--%>

<%-- https://getbootstrap.com/docs/4.1/examples/cover/ --%>

<%@ page import="model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="/includes/header.jsp"%>

<body class="text-center">
<div class="events-container">
    <div style="min-height: 100%" class="p-3">
        <header class="cover-container d-flex mx-auto flex-column masthead mb-auto pl-3 pr-3">
            <div class="inner">
                <h3 class="masthead-brand">Sláinte</h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link" href="index.jsp">Home</a>
                    <a class="nav-link active" href="Event?function=viewAllLiveEvent">Events</a>
                    <a class="nav-link" href="signin.jsp">Sign In</a>
                    <% User user = (User)session.getAttribute("SKUSER"); %>
                    <a class="nav-link" href="
                    <% if (user!= null){
                        out.println("UserManagement?function=viewUser&id=" + user.getId());
                    } else {
                        out.println("/index.jsp");
                    } %>">
                        <% if (user!= null){
                            out.println("Welcome " + user.getFirstName());
                        } else { out.println("Guest Account");
                        } %>
                    </a>
                </nav>
            </div>
        </header>
        <div class="landing padding-top">
            <h1 class="cover-heading">Events</h1>
            <div class="container events-text">
                <div class="row">
                    <!-- https://www.codeply.com/go/jbcgzs2Nzq/bootstrap-4-equal-height-cards -->
                    <c:forEach items="${business}" var="business">
                        <c:forEach items="${business.event}" var="event">
                            <div class="col-lg-4 col-md-6 py-1">
                                <div class="card card-body h-25 card-body-background events-link hvr-underline-from-center"
                                     onclick="location.href='Event?function=viewLiveEvent&id=<c:out value="${event.getId()}"/>';">
                                    <h5 class="card-title">${event.name}</h5>
                                    <p class="card-text">${business.name}</p>
                                    <p class="card-text">
                                        <small>
                                            <!-- https://www.tutorialspoint.com/jsp/jstl_format_formatdate_tag.htm -->
                                            Date: <fmt:formatDate pattern = "dd-MM-yyyy" value="${event.date}" />
                                        </small>
                                    </p>
                                </div>
                            </div>
                        </c:forEach>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<div class="events-container">--%>
    <%--<!-- https://publish.twitter.com/# -->--%>
    <%--<a width="280" height="300" class="twitter-timeline" data-lang="en" data-theme="light" href="https://twitter.com/EventsSlainte?ref_src=twsrc%5Etfw">Tweets by EventsSlainte</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>--%>
<%--</div>--%>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>

<%@ include file="/includes/footer.jsp"%>

