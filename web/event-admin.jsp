<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 15/02/2019
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page import="model.Business" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="/includes/dashboardheader.jsp"%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        height: 40%;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Event Management</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#addModal">
                        <span data-feather="plus"></span>
                        Add Event
                    </button>
                </div>
            </div>
            <div class="container join-container">
                <table class="table table-hover" id="eventDisplay">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Created By</th>
                        <th scope="col">Event Name</th>
                        <th scope="col">Event Date</th>
                        <th scope="col">Max Participants</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${business}" var="business" varStatus="loop">
                        <c:forEach items="${business.event}" var="event">
                            <tr>
                                <td onclick="location.href='Event?function=viewEvent&id=<c:out value="${event.getId()}"/>';" class="align-middle td_link">${business.name}</td>
                                <td onclick="location.href='Event?function=viewEvent&id=<c:out value="${event.getId()}"/>';" class="align-middle td_link">${event.name}</td>
                                <td onclick="location.href='Event?function=viewEvent&id=<c:out value="${event.getId()}"/>';" class="align-middle td_link">${event.date}</td>
                                <td onclick="location.href='Event?function=viewEvent&id=<c:out value="${event.getId()}"/>';" class="align-middle td_link">
                                    <c:set var = "participants" scope = "session" value = "${event.participants}"/>
                                    <c:choose>
                                        <c:when test="${participants <= 1}">
                                            Full
                                        </c:when>
                                        <c:otherwise>
                                            ${event.participants}
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="align-middle">
                                    <div class="row">
                                        <div class="btn-toolbar mr-2">
                                            <%-- https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link --%>
                                            <c:set var = "participants" scope = "session" value = "${event.participants}"/>
                                            <c:choose>
                                                <c:when test="${participants <= 1}">
                                                    <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Event is full">
                                                        <button class="btn btn-sm btn-outline-danger" disabled style="pointer-events: none;">
                                                            Join Event
                                                        </button>
                                                    </span>
                                                </c:when>
                                                <c:otherwise>
                                                    <button id="event${event.id}" data-event-id="${event.id}" data-event-participants="${event.participants}" class="btn btn-sm btn-outline-success open-join-dialog" data-toggle="modal" data-target="#joinModal">
                                                        Join Event
                                                    </button>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
                <!-- The Add Modal -->
                <div class="modal fade" id="addModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Add Event</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Event?function=addEvent" method="post">
                                    <input type="hidden" name="action" value="add">
                                    <label for="inputBusinessName" class="sr-only">Business Name</label>
                                    <select id="inputBusinessName" name="businessId" class="form-control" placeholder="Business Name" required autofocus>
                                        <c:forEach items="${SKALLBUSINESS}" var="business">
                                            <option value="${business.getId()}">${business.name}</option>
                                        </c:forEach>
                                    </select><br>
                                    <label for="inputEventName" class="sr-only">Event Name</label>
                                    <input type="text" id="inputEventName" name="eventName" class="form-control" placeholder="Event Name" required><br>
                                    <label for="inputEventDesc" class="sr-only">Event Description</label>
                                    <input type="text" id="inputEventDesc" name="eventDesc" class="form-control" placeholder="Event Description" required><br>
                                    <label for="inputEventDate" class="sr-only">Event Date</label>
                                    <input type="date" min="2019-02-17" id="inputEventDate" name="eventDate" class="form-control" placeholder="Event Date" required><br>
                                    <label for="inputEventPart" class="sr-only">Event Participants</label>
                                    <input type="number" id="inputEventPart" name="eventPart" class="form-control" placeholder="Event Participants" required><br>
                                    <p><i>${message}</i></p>
                                    <button class="btn btn-block btn-secondary" type="submit">Add Event</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- The Join Modal -->
                <div class="modal fade" id="joinModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Join Event</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Event?function=addStage" method="post">
                                    <input type="hidden" name="action" value="add">
                                    <h6>Select business to join this event</h6>
                                    <input type="hidden" name="eventId" id="eventIdStage" value="">
                                    <input type="hidden" name="eventPart" id="eventPartStage" value="">
                                    <label for="inputBusinessNameStage" class="sr-only">Business Name</label>
                                    <select id="inputBusinessNameStage" name="businessId" class="form-control" placeholder="Business Name" required autofocus>
                                        <c:forEach items="${SKALLBUSINESS}" var="business">
                                            <option value="${business.getId()}">${business.name}</option>
                                        </c:forEach>
                                    </select>
                                    <p><i>${message}</i></p>
                                    <button class="btn btn-block btn-secondary" type="submit">Join Event</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger join-button" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    function tweetTweet() {
        var url = "https://twitter.com/intent/tweet";
        var text = document.getElementById("my-tweet").value;
        var hashtags = "pubcrawl,bar,food,event";
        var via = "EventsSlainte";
        window.open(url+"?text="+text+";hashtags="+hashtags+";via="+via,"","width=300,height=300");
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip(
            {container:'body', trigger: 'hover', placement:"top"}
        );
    });

    feather.replace()

    // https://stackoverflow.com/questions/32378590/set-date-input-fields-max-date-to-today

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("inputEventDate").setAttribute("min", today);


    $(document).on("click", ".open-join-dialog", function () {

        var myEventId = ($(this).data("event-id"));
        var myEventParticipants = ($(this).data("event-participants"));

        $(".modal-body #eventIdStage").val( myEventId );
        $(".modal-body #eventPartStage").val( myEventParticipants );

    });

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s&libraries=places&callback=initMap" async defer></script>

<%@ include file="/includes/dashboardfooter.html"%>
