<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 14/11/2018
  Time: 13:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page import="model.Business" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/includes/dashboardheader.jsp"%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map, #map-2 {
        height: 30%;
    }

</style>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events
                        </a>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Business Management</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#addModal">
                        <span data-feather="plus"></span>
                        Add Business
                    </button>
                </div>
            </div>
            <!-- https://www.w3schools.com/bootstrap4/bootstrap_modal.asp -->
            <div class="container">
                <div class="card-columns">
                    <c:forEach items="${SKALLBUSINESS}" var="business">
                        <div class="card text-center">
                            <div class="card-body">
                                <a id="no-hover" href="Business?function=viewBusiness&id=<c:out value="${business.getId()}"/>">
                                    <input type="hidden" value="${business.getId()}">
                                    <h5 class="card-title">${business.name}</h5>
                                    <p class="card-text">${business.address}</p>
                                    <p class="card-text"><small class="text-muted">Created on: ${business.createDate}</small></p>
                                </a>
                                <div class="row mt-3 justify-content-center">
                                    <div class="btn-toolbar mb-2 mb-md-0 mr-2">
                                        <%-- https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link --%>
                                        <button data-toggle="modal" data-business-id="<c:out value="${business.id}"/>" data-business-name="<c:out value="${business.name}"/>" data-business-address="<c:out value="${business.address}"/>" data-target="#editModal" class="open-updateDialog btn btn-sm btn-outline-secondary">
                                            Edit
                                        </button>
                                    </div>
                                    <div class="btn-toolbar mb-2 mb-md-0 ml-2">
                                        <button class="btn btn-sm btn-outline-danger"
                                                onclick="if (confirm('Are you sure you want to delete? This cannot be undone.'))
                                                { location.href='Business?function=deleteBusiness&id=<c:out value="${business.getId()}"/>'; }
                                                else { return false; }">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <!-- The Add Modal -->
                <div class="modal fade" id="addModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Add Business</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Business?function=addBusiness" method="post">
                                    <input type="hidden" name="action" value="add">
                                    <input type="hidden" name="userId" <%=user = (User)session.getAttribute("SKUSER") %> value="<%= user.getId()%>">
                                    <label for="inputBusinessName" class="sr-only">Business Name</label>
                                    <input type="text" id="inputBusinessName" name="name" class="form-control map-search" placeholder="Business Name" required autofocus><br>
                                    <label for="inputBusinessAddress" class="sr-only">Business Address</label>
                                    <input type="text" id="inputBusinessAddress" name="address" class="form-control map-search" placeholder="Business Address" required>
                                    <input type="hidden" id="inputLat" name="lat" value="">
                                    <input type="hidden" id="inputLong" name="long" value="">
                                    <input type="hidden" id="inputPlaceId" name="placeIdMaps" value="">
                                    <p><i>${message}</i></p>
                                    <br>
                                    <div id="map"></div>
                                    <button class="btn btn-block btn-secondary" type="submit">Add Business</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- The Edit Modal -->
                <div class="modal fade" id="editModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Business</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Business?function=updateBusiness" method="post">
                                    <input type="hidden" id="businessId" name="updateBusinessId" value="">
                                    <label for="inputBusinessNameUpdate" class="sr-only">Business Name</label>
                                    <input type="text" id="inputBusinessNameUpdate" name="updateBusinessName" class="form-control map-search-update" placeholder="Business Name" required autofocus><br>
                                    <label for="inputBusinessAddressUpdate" class="sr-only">Business Address</label>
                                    <input type="text" id="inputBusinessAddressUpdate" name="updateBusinessAddress" class="form-control map-search-update" placeholder="Business Address" required>
                                    <input type="hidden" id="inputLatUpdate" name="updateLat" value="">
                                    <input type="hidden" id="inputLongUpdate" name="updateLong" value="">
                                    <input type="hidden" id="inputPlaceIdUpdate" name="updatePlaceIdMaps" value="">
                                    <p><i>${message}</i></p>
                                    <br>
                                    <div id="map-2"></div>
                                    <button class="btn btn-block btn-secondary" type="submit">Update Business</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()


    // https://developers.google.com/maps/documentation/javascript/examples/place-search

    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var map;
    var service;
    var infowindow;

    /*
        https://stackoverflow.com/questions/15730913/concatenate-multiple-html-text-inputs-with-stored-variable
        https://stackoverflow.com/questions/7484057/jquery-how-to-write-text-values-to-a-hidden-field-for-later-use
        https://stackoverflow.com/questions/14042193/how-to-trigger-an-event-in-input-text-after-i-stop-typing-writing
        https://stackoverflow.com/questions/11456862/get-a-json-file-from-url-and-display
    */
    $(".map-search").keyup(function() {
        delay(function() {
            var businessName = document.getElementById('inputBusinessName');
            var businessAddress = document.getElementById('inputBusinessAddress');
            var str = businessName.value + ' ' + businessAddress.value;
            var urlStr = businessName.value + businessAddress.value;
            initMap(str);

            $.ajax({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + urlStr + '&key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s',
                type: 'GET',
                dataType: "json",
                success: displayAll
            });

            function displayAll(data){
                var placeIdMap = data.results[0].place_id;
                var lat = data.results[0].geometry.location.lat;
                var long = data.results[0].geometry.location.lng;
                $(".modal-body #inputLat").val( lat );
                $(".modal-body #inputLong").val( long );
                $(".modal-body #inputPlaceId").val( placeIdMap );
            }

        }, 500 );
    });

    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();


    function initMap(str) {
        var ireland = new google.maps.LatLng(53.350140, -6.266155);

        infowindow = new google.maps.InfoWindow();

        map = new google.maps.Map(
            document.getElementById('map'), {center: ireland, zoom: 15});

        var request = {
            query: str,
            fields: ['name', 'geometry', 'place_id'],
        };

        service = new google.maps.places.PlacesService(map);

        service.findPlaceFromQuery(request, function(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }

                map.setCenter(results[0].geometry.location);
            }
        });
    }

    function initMapEdit(str) {
        var ireland = new google.maps.LatLng(53.350140, -6.266155);

        infowindow = new google.maps.InfoWindow();

        map = new google.maps.Map(
            document.getElementById('map-2'), {center: ireland, zoom: 15});

        var request = {
            query: str,
            fields: ['name', 'geometry', 'place_id'],
        };

        service = new google.maps.places.PlacesService(map);

        service.findPlaceFromQuery(request, function(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }

                map.setCenter(results[0].geometry.location);
            }
        });
    }

    function createMarker(place) {
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }

    /*
    https://stackoverflow.com/questions/10626885/passing-data-to-a-bootstrap-modal
     */

    $(document).on("click", ".open-updateDialog", function () {
        var updateBusinessId = $(this).data('business-id');
        var updateBusinessName = $(this).data('business-name');
        var updateBusinessAddress = $(this).data('business-address');
        var str = $(this).data('business-name') + ' ' + $(this).data('business-address');
        $(".modal-body #businessId").val( updateBusinessId );
        $(".modal-body #inputBusinessNameUpdate").val( updateBusinessName );
        $(".modal-body #inputBusinessAddressUpdate").val( updateBusinessAddress );
        initMapEdit(str);
    });

    $(".map-search-update").keyup(function() {
        delay(function() {
            var businessNameUpdate = document.getElementById('inputBusinessNameUpdate');
            var businessAddressUpdate = document.getElementById('inputBusinessAddressUpdate');
            var str = businessNameUpdate.value + ' ' + businessAddressUpdate.value;
            var urlStr = businessNameUpdate.value + businessAddressUpdate.value;
            initMap(str);

            $.ajax({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + urlStr + '&key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s',
                type: 'GET',
                dataType: "json",
                success: displayAll
            });

            function displayAll(data){
                var placeIdMap = data.results[0].place_id;
                var lat = data.results[0].geometry.location.lat;
                var long = data.results[0].geometry.location.lng;
                $(".modal-body #inputLatUpdate").val( lat );
                $(".modal-body #inputLongUpdate").val( long );
                $(".modal-body #inputPlaceIdUpdate").val( placeIdMap );
            }

        }, 500 );
    });

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s&libraries=places&callback=initMap" async defer></script>

<%@ include file="/includes/dashboardfooter.html"%>