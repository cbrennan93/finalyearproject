<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 02/11/2018
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/includes/dashboardheader.jsp"%>
<% Vector<User> allUsers = (Vector<User>) session.getAttribute("SKALLUSERS");%>


<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->
<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events
                        </a>
                    </li>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Dashboard</h1>
                <%--<div class="btn-toolbar mb-2 mb-md-0">--%>
                    <%--<div class="btn-group mr-2">--%>
                        <%--<button class="btn btn-sm btn-outline-secondary">Share</button>--%>
                        <%--<button class="btn btn-sm btn-outline-secondary">Export</button>--%>
                    <%--</div>--%>
                    <%--<button class="btn btn-sm btn-outline-secondary dropdown-toggle">--%>
                        <%--<span data-feather="calendar"></span>--%>
                        <%--This week--%>
                    <%--</button>--%>
                <%--</div>--%>
            </div>
            <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>
<% Integer createDate = (Integer) request.getAttribute("alldays"); %>
<!-- Graphs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script>
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
            datasets: [{
                data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                lineTension: 0,
                backgroundColor: 'transparent',
                borderColor: '#007bff',
                borderWidth: 4,
                pointBackgroundColor: '#007bff'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            },
            legend: {
                display: false,
            }
        }
    });
</script>
<%@ include file="/includes/dashboardfooter.html"%>

