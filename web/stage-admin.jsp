<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 22/02/2019
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="/includes/dashboardheader.jsp"%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">${eventName}</h1>
            </div>

            <!-- https://www.w3schools.com/bootstrap4/bootstrap_modal.asp -->

            <div class="container">
                <table class="table table-hover" id="myTable">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Order #</th>
                        <th scope="col">Location</th>
                        <th scope="col">Latitude</th>
                        <th scope="col">Longitude</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${stage}" var="stage">
                        <tr>
                            <td class="align-middle">${stage.stageNumber}</td>
                            <td class="align-middle">${stage.business.name}</td>
                            <td class="align-middle myTableLat">${stage.business.lat}</td>
                            <td class="align-middle myTableLong">${stage.business.lng}</td>
                            <td class="align-middle">
                                <div class="row">
                                    <div class="btn-toolbar mr-2">
                                            <%-- https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link --%>
                                        <button disabled class="btn btn-sm btn-outline-secondary">
                                            Edit
                                        </button>
                                    </div>
                                    <div class="btn-toolbar mr-2">
                                            <%-- http://www.informit.com/articles/article.aspx?p=24123&seqNum=4 --%>
                                        <button disabled class="btn btn-sm btn-outline-danger">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <style>
                #map {
                    height: 40%;
                    float: left;
                    width: 100%;
                }
            </style>
            <div id="map"></div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<script>
    // https://stackoverflow.com/questions/1149958/jquery-count-number-of-rows-in-a-table
    var lastRow = $('#myTable tbody tr').length - 1;
    var firstLat = $('#myTable tbody tr .myTableLat')[0].innerHTML;
    var firstLong = $('#myTable tbody tr .myTableLong')[0].innerHTML;
    var lastLat = $('#myTable tbody tr .myTableLat')[lastRow].innerHTML;
    var lastLong = $('#myTable tbody tr .myTableLong')[lastRow].innerHTML;
    var displayLat = parseFloat(firstLat);
    var displayLong = parseFloat(firstLong);

    // https://developers.google.com/maps/documentation/javascript/examples/directions-waypoints

    initMap();
    function initMap() {

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: {lat: displayLat, lng: displayLong}
        });
        directionsDisplay.setMap(map);

        calculateAndDisplayRoute(directionsService, directionsDisplay);

    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        // https://stackoverflow.com/questions/6593473/lat-long-array-as-markers-on-google-maps-api-v3
        var waypts = [];
        if ( lastRow > 0 ) {
            for (var i = 1; i < lastRow; i++) {
                var waypointLat = parseFloat($('#myTable tbody tr .myTableLat')[i].innerHTML);
                var waypointLong = parseFloat($('#myTable tbody tr .myTableLong')[i].innerHTML);
                waypts.push({
                    location: {
                        lat: waypointLat,
                        lng: waypointLong },
                    stopover: true
                });
            }
        }
        directionsService.route({

            // https://stackoverflow.com/questions/46935093/google-maps-error-not-a-latlng-or-latlngliteral-invalidvalueerror
            origin: {
                lat: parseFloat(firstLat),
                lng: parseFloat(firstLong)
            },
            destination: {
                lat: parseFloat(lastLat),
                lng: parseFloat(lastLong)
            },
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: 'WALKING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                for (var i = 0; i < route.legs.length; i++ ) {
                    console.log("Distance from "+ (i+1) +" to "+ (i+2) +" is " + route.legs[i].distance.text);
                }
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s&callback=initMap">
</script>

<%@ include file="/includes/dashboardfooter.html"%>

