<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 03/11/2018
  Time: 14:19
  To change this template use File | Settings | File Templates.
--%>

<!-- https://getbootstrap.com/docs/4.1/examples/sign-in/ -->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicons/favicon.ico">

    <title>My FYP</title>

    <!-- Bootstrap core CSS -->
    <link href="../styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../styles/signin.css" rel="stylesheet">
    <link href="../styles/main.css" rel="stylesheet">
</head>

<body class="home-container text-center">
<form class="form-signin" action="UserManagement?function=addUser" method="post">
    <input type="hidden" name="action" value="add">
    <img class="mb-4" src="../img/logowhite.png" alt="" width="72" height="72">
    <h1 class="h3 mb-3 text-light font-weight-normal">Please register</h1>
    <label for="inputFirstName" class="sr-only">First Name</label>
    <input type="text" id="inputFirstName" name="firstName" class="form-control" placeholder="First Name" required autofocus>
    <label for="inputLastName" class="sr-only">First Name</label>
    <input type="text" id="inputLastName" name="lastName" class="form-control" placeholder="Last Name" required>
    <label for="inputEmailReg" class="sr-only">Email address</label>
    <input type="email" id="inputEmailReg" name="email" class="form-control" placeholder="Email Address" required>
    <label for="inputDate" class="sr-only">Date of Birth</label>
    <input type="date" id="inputDate" name="birthDate" class="form-control" placeholder="Date of Birth" required>
    <div class="input-group">
        <div class="input-group-prepend">
            <label class="input-group-text" id="userTypeLabel" for="inputUserType">User Type</label>
        </div>
        <select class="custom-select" id="inputUserType" name="userType" required>
            <option value="RegUser">Please Choose...</option>
            <option value="RegUser">Regular User</option>
            <option value="Admin">Business Owner</option>
        </select>
    </div>
    <label for="inputPasswordReg" class="sr-only">Password</label>
    <input type="password" id="inputPasswordReg" name="password" class="form-control" placeholder="Password" required>
    <label for="inputConfirmPassword" class="sr-only">Confirm Password</label>
    <input type="password" id="inputConfirmPassword" name="passwordConf" class="form-control" placeholder="Confirm Password" required>
    <span style="color:red">${message}</span>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
    <br>
    <a href="signin.jsp">Already registered?</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
</form>
</body>
</html>
