<%--
  Created by IntelliJ IDEA.
  model.User: ciaranbrennan
  Date: 18/10/2018
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>

<%-- https://getbootstrap.com/docs/4.1/examples/cover/ --%>

<%@ page import="model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/includes/header.jsp"%>

<body class="text-center">
<div class="index-container">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <header class="masthead mb-auto">
            <div class="inner">
                <h3 class="masthead-brand">Sláinte</h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link active" href="index.jsp">Home</a>
                    <a class="nav-link" href="Event?function=viewAllLiveEvent">Events</a>
                    <a class="nav-link" href="signin.jsp">Sign In</a>
                    <% User user = (User)session.getAttribute("SKUSER"); %>
                    <a class="nav-link" href="
                    <% if (user!= null){
                        out.println("UserManagement?function=viewUser&id=" + user.getId());
                    } else {
                        out.println("/index.jsp");
                    } %>">
                    <% if (user!= null){
                        out.println("Welcome " + user.getFirstName());
                    } else { out.println("Guest Account");
                    } %>
                    </a>
                </nav>
            </div>
        </header>
        <div class="landing">
            <main role="main" class="inner cover centering">
                <h1 class="cover-heading">Welcome to Sláinte</h1>
                <a href="#section2"><p class="lead"><span style="height: 40px; width: 40px" data-feather="chevrons-down"></span></p></a>
            </main>
        </div>
    </div>
</div>
<div class="index-container" id="section2">
    <div class="container" id="nav-links">
        <div class="d-flex text-padding-top">
            <h1 class="cover-heading">Businesses</h1>
        </div>
        <div class="container events-text">
            <div class="row">
                <!-- https://www.codeply.com/go/jbcgzs2Nzq/bootstrap-4-equal-height-cards -->



            </div>
        </div>
    </div>
</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    feather.replace();
    loadBusinessServlet();
    $('body').css('display', 'none');
    // https://www.w3schools.com/jquery/tryit.asp?filename=tryjquery_eff_animate_smoothscroll
    $(document).ready(function(){

        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
// https://stackoverflow.com/questions/15839854/how-load-servlet-on-index-jsp
    function loadBusinessServlet(){

        $.ajax({
            cache: false,
            url : '/Business?function=viewIndex',
            type: "POST",
            dataType: "json",
            success:function(data){
                for(var i in data) {
                    $('.row').append('<div class="col-lg-4 col-md-6 py-2">\n' +
                        '<div class="card card-body p-5 card-body-background-index events-link hvr-underline-from-center"\n' +
                        'onclick="location.href=\'Business?function=viewBusiness&id=' + data[i].id + "';" + '">\n' +
                        '<h5 class="card-title">' + data[i].name + '</h5>\n' +
                        '<p class="card-text">' + data[i].address + '</p>\n' +
                        '</div>\n' +
                        '</div>');
                }

                $('body').css('display', 'block');

            },
            error: function (event) {

                console.log("ERROR: ", event);
            }
        });
    }

</script>

<%@ include file="/includes/footer.jsp"%>
