<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 19/11/2018
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<%@ include file="/includes/dashboardheader.jsp"%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events
                        </a>
                    </li>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Menu Management</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#addModal">
                        <span data-feather="plus"></span>
                        Add Menu
                    </button>
                </div>
            </div>
            <!-- https://www.w3schools.com/bootstrap4/bootstrap_modal.asp -->
            <div class="container">
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Business Name</th>
                        <th scope="col">Menu Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${business}" var="business">
                        <c:forEach items="${business.menu}" var="menu">
                            <tr>
                                <td onclick="location.href='Business?function=editMenu&id=<c:out value="${menu.getId()}"/>';" class="align-middle td_link">${business.name}</td>
                                <td onclick="location.href='Business?function=editMenu&id=<c:out value="${menu.getId()}"/>';" class="align-middle td_link">${menu.menuName}</td>
                                <td class="align-middle">
                                    <div class="row">
                                        <div class="btn-toolbar mr-2">
                                                <%-- https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link --%>
                                            <button data-toggle="modal" data-menu-id="<c:out value="${menu.id}"/>" data-menu-name="<c:out value="${menu.menuName}"/>" data-business-id="<c:out value="${business.id}"/>" data-business-name="<c:out value="${business.name}"/>" data-target="#editModal" class="open-updateDialog btn btn-sm btn-outline-secondary">
                                                Edit
                                            </button>
                                        </div>
                                        <div class="btn-toolbar mr-2">
                                            <button class="btn btn-sm btn-outline-danger"
                                                    onclick="if (confirm('Are you sure you want to delete? This cannot be undone.'))
                                                            { location.href='Business?function=deleteMenu&id=<c:out value="${menu.getId()}"/>'; }
                                                            else { return false; }">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
                <!-- The Add Modal -->
                <div class="modal fade" id="addModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Add Menu</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Business?function=addMenu" method="post">
                                    <input type="hidden" name="action" value="add">
                                    <label for="inputBusinessName" class="sr-only">Business Name</label>
                                    <select id="inputBusinessName" name="businessId" class="form-control" placeholder="Business Name" required autofocus>
                                        <c:forEach items="${SKALLBUSINESS}" var="business">
                                            <option value="${business.getId()}">${business.name}</option>
                                        </c:forEach>
                                    </select><br>
                                    <label for="inputMenuName" class="sr-only">Menu Name</label>
                                    <input type="text" id="inputMenuName" name="menuName" class="form-control" placeholder="Menu Name" required>
                                    <p><i>${message}</i></p>
                                    <button class="btn btn-block btn-secondary" type="submit">Add Menu</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- The Edit Modal -->
                <div class="modal fade" id="editModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Menu</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Business?function=updateMenu" method="post">
                                    <input type="hidden" id="menuId" name="updateMenuId" value="">
                                    <label for="inputBusinessNameUpdate" class="sr-only">Business Name</label>
                                    <select id="inputBusinessNameUpdate" name="updateBusinessId" class="form-control" placeholder="Business Name" required autofocus>
                                        <c:forEach items="${SKALLBUSINESS}" var="business">
                                            <option value="${business.getId()}">${business.name}</option>
                                        </c:forEach>
                                    </select><br>
                                    <label for="inputMenuNameUpdate" class="sr-only">Menu Name</label>
                                    <input type="text" id="inputMenuNameUpdate" name="updateMenuName" class="form-control" placeholder="Menu Name" required autofocus><br>
                                    <p><i>${message}</i></p>
                                    <button class="btn btn-block btn-secondary" type="submit">Update Menu</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
    /*
    https://stackoverflow.com/questions/10626885/passing-data-to-a-bootstrap-modal
    https://www.daftlogic.com/information-programmatically-preselect-dropdown-using-javascript.htm
     */

    $(document).on("click", ".open-updateDialog", function () {
        var myBusinessName = $(this).data('business-name');
        var myMenuId = $(this).data('menu-id')
        var myMenuName = $(this).data('menu-name')

        function setSelectedIndex(s, v) {

            for ( var i = 0; i < s.options.length; i++ ) {

                if ( s.options[i].text === v ) {

                    s.options[i].selected = true;

                    return;

                }

            }

        }
        setSelectedIndex(document.getElementById('inputBusinessNameUpdate'),myBusinessName);
        $(".modal-body #menuId").val( myMenuId );
        $(".modal-body #inputMenuNameUpdate").val( myMenuName );
    });
</script>

<%@ include file="/includes/dashboardfooter.html"%>
