<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 27/12/2018
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="/includes/dashboardheader.jsp"%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events
                        </a>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Opening Hours Management</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#addModal">
                        <span data-feather="plus"></span>
                        Add Opening Hours
                    </button>
                </div>
            </div>
            <div class="container join-container">
                <p style="color:red">${message}</p>
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Business Name</th>
                        <th scope="col">Opening Hours?</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${business}" var="business">
                        <tr>
                            <td class="align-middle">${business.name}</td>
                            <td class="align-middle">Yes</td>
                            <td class="align-middle">
                                <div class="row">
                                    <div class="btn-toolbar">
                                        <button id="viewHoursButton" class="btn btn-sm btn-outline-secondary" data-business-id="<c:out value="${business.id}"/>"  data-toggle="modal" data-target="#viewModal">
                                            View Hours
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <!-- The Add Modal -->
                <div class="modal fade" id="addModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Add Hours</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <form action="Business?function=addHours" method="post">
                                        <input type="hidden" name="action" value="add">
                                        <label for="inputBusinessName" class="sr-only">Business Name</label>
                                        <select id="inputBusinessName" name="businessId" class="form-control" placeholder="Business Name" required autofocus>
                                            <c:forEach items="${SKALLBUSINESS}" var="business">
                                                <option value="${business.getId()}">${business.name}</option>
                                            </c:forEach>
                                        </select><br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputMonOpen" class="col-form-label"><b>Monday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputMonOpen" name="monOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputMonClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputMonClose" name="monClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputTuesOpen" class="col-form-label"><b>Tuesday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputTuesOpen" name="tuesOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputTuesClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputTuesClose" name="tuesClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputWedOpen" class="col-form-label"><b>Wednesday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputWedOpen" name="wedOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputWedClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputWedClose" name="wedClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputThursOpen" class="col-form-label"><b>Thursday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputThursOpen" name="thursOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputThursClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputThursClose" name="thursClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputFriOpen" class="col-form-label"><b>Friday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputFriOpen" name="friOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputFriClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputFriClose" name="friClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputSatOpen" class="col-form-label"><b>Saturday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputSatOpen" name="satOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputSatClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputSatClose" name="satClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="inputSunOpen" class="col-form-label"><b>Sunday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputSunOpen" name="sunOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label  for="inputSunClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" id="inputSunClose" name="sunClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <button class="btn btn-block btn-secondary" type="submit">Add Hours</button>
                                        <br>
                                    </form>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- The view modal-->
                <div class="modal fade" id="viewModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">View Hours</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <form action="Business?function=addHours" method="post">
                                        <input type="hidden" name="action" value="add">
                                        <label for="viewName" class="sr-only">Business Name</label>
                                        <input disabled type="text" id="viewName" name="viewName" class="form-control" placeholder="Business Name" required ><br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewMonOpen" class="col-form-label"><b>Monday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewMonOpen" name="viewMonOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewMonClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewMonClose" name="viewMonClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewTuesOpen" class="col-form-label"><b>Tuesday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewTuesOpen" name="viewTuesOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewTuesClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewTuesClose" name="viewTuesClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewWedOpen" class="col-form-label"><b>Wednesday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewWedOpen" name="viewWedOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewWedClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewWedClose" name="viewWedClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewThursOpen" class="col-form-label"><b>Thursday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewThursOpen" name="viewThursOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewThursClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewThursClose" name="viewThursClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewFriOpen" class="col-form-label"><b>Friday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewFriOpen" name="viewFriOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewFriClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewFriClose" name="viewFriClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewSatOpen" class="col-form-label"><b>Saturday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewSatOpen" name="viewSatOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewSatClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewSatClose" name="viewSatClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="viewSunOpen" class="col-form-label"><b>Sunday:</b></label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewSunOpen" name="viewSunOpen" class="form-control" placeholder="Open" required><br>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="viewSunClose" class="col-form-label">to</label>
                                            </div>
                                            <div class="col-md-4">
                                                <input value="" disabled type="text" id="viewSunClose" name="sunClose" class="form-control" placeholder="Close" required><br>
                                            </div>
                                        </div>
                                        <br>
                                    </form>
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()

    // https://stackoverflow.com/questions/15839854/how-load-servlet-on-index-jsp
    $(document).on("click", "#viewHoursButton", function () {

        var myBusinessId = $(this).data('business-id');

        $.ajax({
            url : '/Business?function=viewHours',
            data: {"businessId": myBusinessId},
            type: "GET",
            success: function(response) {

                console.log(response);

                $(".modal-body #viewName").val(response.name);

                $(".modal-body #viewMonOpen").val(response.hours[0].open);
                $(".modal-body #viewMonClose").val(response.hours[0].close);
                $(".modal-body #viewTuesOpen").val(response.hours[1].open);
                $(".modal-body #viewTuesClose").val(response.hours[1].close);
                $(".modal-body #viewWedOpen").val(response.hours[2].open);
                $(".modal-body #viewWedClose").val(response.hours[2].close);
                $(".modal-body #viewThursOpen").val(response.hours[3].open);
                $(".modal-body #viewThursClose").val(response.hours[3].close);
                $(".modal-body #viewFriOpen").val(response.hours[4].open);
                $(".modal-body #viewFriClose").val(response.hours[4].close);
                $(".modal-body #viewSatOpen").val(response.hours[5].open);
                $(".modal-body #viewSatClose").val(response.hours[5].close);
                $(".modal-body #viewSunOpen").val(response.hours[6].open);
                $(".modal-body #viewSunClose").val(response.hours[6].close);

            },
            error: function (event) {

                console.log("ERROR: ", event);
            }
        });


    });
</script>

<%@ include file="/includes/dashboardfooter.html"%>
