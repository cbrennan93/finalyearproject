<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 11/02/2019
  Time: 13:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="model.User" %>
<%@ page import="java.util.Vector" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="/includes/dashboardheader.jsp"%>

<!-- https://getbootstrap.com/docs/4.1/examples/dashboard/ -->

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="dashboard.jsp">
                            <span data-feather="home"></span>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllBusiness">
                            <span data-feather="briefcase"></span>
                            Business
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="Business?function=viewAllMenu">
                            <span data-feather="file"></span>
                            Menu <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Business?function=viewAllHour">
                            <span data-feather="clock"></span>
                            Opening Hours
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Event?function=viewAllEvent">
                            <span data-feather="calendar"></span>
                            Events
                        </a>
                    </li>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="users"></span>--%>
                            <%--Users--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="bar-chart-2"></span>--%>
                            <%--Reports--%>
                        <%--</a>--%>
                    <%--</li>--%>
                </ul>

                <%--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--%>
                    <%--<span>Saved reports</span>--%>
                    <%--<a class="d-flex align-items-center text-muted" href="#">--%>
                        <%--<span data-feather="plus-circle"></span>--%>
                    <%--</a>--%>
                <%--</h6>--%>
                <%--<ul class="nav flex-column mb-2">--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Current month--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Last quarter--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Social engagement--%>
                        <%--</a>--%>
                    <%--</li>--%>
                    <%--<li class="nav-item">--%>
                        <%--<a class="nav-link" href="#">--%>
                            <%--<span data-feather="file-text"></span>--%>
                            <%--Year-end sale--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">${businessName} ${menuName} Menu</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#addModal">
                        <span data-feather="plus"></span>
                        Add Item
                    </button>
                </div>
            </div>

            <!-- https://www.w3schools.com/bootstrap4/bootstrap_modal.asp -->

            <div class="container">
                <table class="table table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Category</th>
                        <th scope="col">Price(€)</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${items}" var="item">
                            <tr>
                                <td class="align-middle">${item.itemName}</td>
                                <td class="align-middle">${item.itemDesc}</td>
                                <td class="align-middle">${item.itemCat}</td>
                                <td class="align-middle">${item.itemPrice}</td>
                                <td class="align-middle">
                                    <div class="row">
                                        <div class="btn-toolbar mr-2">
                                                <%-- https://stackoverflow.com/questions/2906582/how-to-create-an-html-button-that-acts-like-a-link --%>
                                            <button class="open-updateDialog btn btn-sm btn-outline-secondary"
                                                    data-toggle="modal"
                                                    data-item-id="<c:out value="${item.id}"/>"
                                                    data-item-name="<c:out value="${item.itemName}"/>"
                                                    data-item-desc="<c:out value="${item.itemDesc}"/>"
                                                    data-item-cat="<c:out value="${item.itemCat}"/>"
                                                    data-item-price="<c:out value="${item.itemPrice}"/>"
                                                    data-menu-id="<c:out value="${item.menuId}"/>"
                                                    data-target="#editModal">
                                                Edit
                                            </button>
                                        </div>
                                        <div class="btn-toolbar mr-2">
                                            <%-- http://www.informit.com/articles/article.aspx?p=24123&seqNum=4 --%>
                                            <button class="btn btn-sm btn-outline-danger"
                                                    onclick="if (confirm('Are you sure you want to delete? This cannot be undone.'))
                                                            { location.href='Business?function=deleteItem&id=<c:out value="${item.getId()}"/>&menuId=<c:out value="${item.menuId}"/>'; }
                                                            else { return false; }">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <!-- The Add Modal -->
                <div class="modal fade" id="addModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Add Item</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Business?function=addItem" method="post">
                                    <input type="hidden" name="menuId" value="${addItemMenuId}">
                                    <label for="inputItemName" class="sr-only">Item Name</label>
                                    <input type="text" id="inputItemName" name="itemName" class="form-control" placeholder="Item Name" required autofocus><br>
                                    <label for="inputItemDesc" class="sr-only">Item Description</label>
                                    <input type="text" id="inputItemDesc" name="itemDesc" class="form-control" placeholder="Item Description"><br>
                                    <label for="inputItemCat" class="sr-only">Item Category</label>
                                    <input type="text" id="inputItemCat" name="itemCat" class="form-control" placeholder="Item Category" required><br>
                                    <label for="inputItemPrice" class="sr-only">Item Description</label>
                                    <input type="number" id="inputItemPrice" name="itemPrice" class="form-control" placeholder="Item Price" step=".01" required>
                                    <p><i>${message}</i></p>
                                    <button class="btn btn-block btn-secondary" type="submit">Add Item</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- The Edit Modal -->
                <div class="modal fade" id="editModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Menu</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form action="Business?function=updateItem" method="post">
                                    <input type="hidden" id="itemId" name="updateItemId" value="">
                                    <input type="hidden" id="itemMenuId" name="updateMenuId" value="">
                                    <label for="inputItemNameUpdate" class="sr-only">Item Name</label>
                                    <input type="text" id="inputItemNameUpdate" name="updateItemName" class="form-control" placeholder="Item Name" required autofocus><br>
                                    <label for="inputItemDescUpdate" class="sr-only">Item Description</label>
                                    <input type="text" id="inputItemDescUpdate" name="updateItemDesc" class="form-control" placeholder="Item Description"><br>
                                    <label for="inputItemCatUpdate" class="sr-only">Item Category</label>
                                    <input type="text" id="inputItemCatUpdate" name="updateItemCat" class="form-control" placeholder="Item Category" required><br>
                                    <label for="inputItemPriceUpdate" class="sr-only">Item Description</label>
                                    <input type="number" id="inputItemPriceUpdate" name="updateItemPrice" class="form-control" placeholder="Item Price" step=".01" required>
                                    <p><i>${message}</i></p>
                                    <button class="btn btn-block btn-secondary" type="submit">Update Item</button>
                                    <br>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../js/vendor/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
    /*
    https://stackoverflow.com/questions/10626885/passing-data-to-a-bootstrap-modal
    https://www.daftlogic.com/information-programmatically-preselect-dropdown-using-javascript.htm
     */

    $(document).on("click", ".open-updateDialog", function () {

        var myItemId = $(this).data('item-id');
        var myItemName = $(this).data('item-name');
        var myItemDesc = $(this).data('item-desc');
        var myItemCat = $(this).data('item-cat');
        var myItemPrice = $(this).data('item-price');
        var myMenuId = $(this).data('menu-id');

        $(".modal-body #itemId").val( myItemId );
        $(".modal-body #inputItemNameUpdate").val( myItemName );
        $(".modal-body #inputItemDescUpdate").val( myItemDesc );
        $(".modal-body #inputItemCatUpdate").val( myItemCat );
        $(".modal-body #inputItemPriceUpdate").val( myItemPrice );
        $(".modal-body #itemMenuId").val( myMenuId );

    });
</script>

<%@ include file="/includes/dashboardfooter.html"%>
