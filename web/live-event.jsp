<%--
  Created by IntelliJ IDEA.
  User: ciaranbrennan
  Date: 27/02/2019
  Time: 21:17
  To change this template use File | Settings | File Templates.
--%>

<%-- https://getbootstrap.com/docs/4.1/examples/cover/ --%>

<%@ page import="model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="/includes/header.jsp"%>

<body class="text-center">
    <div class="events-container">
        <div class="min-height p-3">
            <header class="cover-container d-flex mx-auto flex-column masthead mb-auto pl-3 pr-3">
                <div class="inner">
                    <h3 class="masthead-brand">Sláinte</h3>
                    <nav class="nav nav-masthead justify-content-center">
                        <a class="nav-link" href="index.jsp">Home</a>
                        <a class="nav-link active" href="Event?function=viewAllLiveEvent">Events</a>
                        <a class="nav-link" href="signin.jsp">Sign In</a>
                        <a class="nav-link" href="dashboard.jsp">
                            <%
                                User user = (User)session.getAttribute("SKUSER");
                                if (user!= null) {
                                    out.println("Welcome " + user.getFirstName());
                                } else {
                                    out.println("Guest Account");
                                }
                            %>
                        </a>
                    </nav>
                </div>
            </header>
            <div class="landing padding-top">
                <c:set var = "event" value = "${event}"/>
                <h1 class="cover-heading"><c:out value = "${event.name}"/></h1>
                <h4><fmt:formatDate pattern = "dd-MM-yyyy" value="${event.date}" /></h4>
                <div class="col-md-6 offset-md-3 text-left"><c:out value = "${event.desc}"/></div>
                <div class="container events-text">
                    <h2 class="cover-heading">Route</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-borderless event-list-table" id="myTable">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">Stage</th>
                                    <th scope="col">Start</th>
                                    <th scope="col">Address</th>
                                </tr>
                                </thead>
                                <tbody class="event-list-table">
                                <c:forEach items="${stage}" var="stage">
                                    <tr>
                                        <td class="align-middle">${stage.stageNumber}</td>
                                        <td class="align-middle">${stage.business.name}</td>
                                        <td class="align-middle">${stage.business.address}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script>
    // https://stackoverflow.com/questions/44902684/jsp-list-populate-into-javascript-array
    var array = [];
    var places = [];

    <c:forEach items="${stage}" var="stage">
        c = new Object();
        //name property example
        c.lat= '${stage.business.lat}';
        c.long = '${stage.business.lng}';
        c.placeId = '${stage.business.placeIdMaps}';
        //other properties
        array.push(c);
    </c:forEach>

    // center of map needs a LatLng type and not a placeId
    var firstLat = array[0].lat;
    var firstLong = array[0].long;
    var displayLat = parseFloat(firstLat);
    var displayLong = parseFloat(firstLong);

    // variables for all way points
    var lastRow = array.length - 1;
    var firstPlace = array[0].placeId;
    var lastPlace = array[lastRow].placeId;

    // https://developers.google.com/maps/documentation/javascript/examples/directions-waypoints
    // https://developers.google.com/maps/documentation/javascript/examples/marker-labels
    // https://stackoverflow.com/questions/41374432/how-to-disable-the-satellite-and-street-view-options-in-the-google-map

    initMap();

    function initMap() {

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
            suppressMarkers: true
        });
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 2,
            center: {lat: displayLat, lng: displayLong},
            // street view control
            streetViewControl: false,
            disableDefaultUI: true,
            gestureHandling: 'greedy'
        });
        directionsDisplay.setMap(map);


        calculateAndDisplayRoute(directionsService, directionsDisplay);

        // https://stackoverflow.com/questions/25638834/mutable-variable-is-accessible-from-closure/25638959
        for (var i = 0; i <= array.length; i++) {
            // Closure function
            doThisFunction(i);
        }
        function doThisFunction(i) {
            var request = {
                placeId: array[i].placeId,
                fields: ['name', 'formatted_address', 'place_id', 'geometry']
            };

            var infowindow = new google.maps.InfoWindow();
            var service = new google.maps.places.PlacesService(map);
            service.getDetails(request, function (place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    i += 1;
                    var marker = new google.maps.Marker({
                        map: map,
                        position: place.geometry.location,
                        // marker label
                        label: {
                            color: '#fff',
                            fontSize: '12px',
                            fontWeight: '600',
                            text: i.toString()
                        }
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        infowindow.setContent('<div style="color:black"><strong>' + place.name + '</strong><br>' +
                            place.formatted_address + '</div>');
                        infowindow.open(map, this);
                    });
                }
            });
        }
    }


    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        // https://stackoverflow.com/questions/6593473/lat-long-array-as-markers-on-google-maps-api-v3
        var waypts = [];
        if ( lastRow > 0 ) {
            for (var i = 1; i < lastRow; i++) {
                waypts.push({
                    location: {placeId: array[i].placeId},
                    stopover: true
                });
            }
        }
        directionsService.route({
            // https://stackoverflow.com/questions/46935093/google-maps-error-not-a-latlng-or-latlngliteral-invalidvalueerror
            origin: {
                placeId: firstPlace
            },
            destination: {
                placeId: lastPlace
            },
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: 'WALKING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                for (var i = 0; i < route.legs.length; i++ ) {
                    console.log("Distance from "+ (i+1) +" to "+ (i+2) +" is " + route.legs[i].distance.text);
                }
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA80s6ieIq0pyRW7CMwZgyVNhexP2UiB2s&libraries=places&callback=initMap">
</script>

<%@ include file="/includes/footer.jsp"%>


